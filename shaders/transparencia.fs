#version 130

uniform float Transparencia;
varying vec3 OutLightIntensity;
varying vec4 ShadowCoord;

uniform sampler2D Tex;
uniform float TexBlending;

uniform sampler2D shadowMap;


void main() {
	float visibility = 1.0;
	if( texture( shadowMap,  ShadowCoord.xy ).z  <  ShadowCoord.z-0.05){
		visibility = 0.3;
	}
	gl_FragColor = visibility * vec4(OutLightIntensity, 1.0);
	gl_FragColor.a = Transparencia;
}
