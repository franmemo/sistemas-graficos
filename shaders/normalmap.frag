#version 130

varying vec3 LightDir;
varying vec2 TexCoord;
varying vec3 ViewDir;
varying vec4 ShadowCoord;


uniform sampler2D Tex;
uniform sampler2D NormalMap;
uniform sampler2D shadowMap;

uniform float TexBlending;

struct LightInfo {
  vec4 Position;  // Light position in eye coords.
  vec3 Intensity; // A,D,S intensity
};

uniform LightInfo Light;

struct MaterialInfo {
  vec3 Ka;            // Ambient reflectivity
  vec3 Ks;            // Specular reflectivity
  vec3 Kd;			  // Diffuse reflectivity
  float Shininess;    // Specular shininess factor
};

uniform MaterialInfo Material;


vec3 phongModel( vec3 norm, vec3 diffR ) {
	float visibilidad = 1.0;
	float x, y;
	
	for (x = -0.5; x<= 0.5; x += 1.0 ) {
       	for (y = -0.5; y<= 0.5; y += 1.0 ) {
       		if (texture( shadowMap, ShadowCoord.xy + vec2(x,y)/700).z < ShadowCoord.z - 0.002)
       			visibilidad -= 0.4 / 4. ;
		}
    }

	vec3 r = reflect( -LightDir, norm );
	vec3 ambient = Light.Intensity * Material.Ka;
	float sDotN = max( dot(LightDir, norm), 0.0 );
	vec3 diffuse = Light.Intensity * diffR * sDotN;
	vec3 spec = vec3(0.0);
	if( sDotN > 0.0 )
		spec = Light.Intensity * Material.Ks *
		pow( max( dot(r,ViewDir), 0.0 ),
		Material.Shininess );
	return ambient + visibilidad * (diffuse + spec);
}

float rand(vec2 co) {
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

void main() {
    // Lookup the normal from the normal map
	vec4 normal = texture( NormalMap, TexCoord );
        
	float visibilidad = 1.0;
	float x, y;
	

	for (x = -0.5; x<= 0.5; x += 1.0 ) {
       	for (y = -0.5; y<= 0.5; y += 1.0 ) {
       		if (texture( shadowMap, ShadowCoord.xy + vec2(x,y)/700).z < ShadowCoord.z - 0.002)
       			visibilidad -= 0.4 / 4. ;
		}
    }

    vec4 texColor = texture( Tex, TexCoord );

    gl_FragColor = vec4( visibilidad * mix(phongModel(normal.xyz, texColor.rgb), visibilidad * texture( Tex, TexCoord ).rgb, TexBlending), 1.0 );
}
