#version 130

varying vec3 Position;
varying vec3 Normal;
varying vec2 TexCoord;
varying vec4 ShadowCoord;

uniform sampler2D Tex;
uniform sampler2D shadowMap;

struct LightInfo {
  vec4 Position;  // Light position in eye coords.
  vec3 Intensity; // A,D,S intensity
};

uniform LightInfo Light;

struct MaterialInfo {
  vec3 Ka;            // Ambient reflectivity
  vec3 Kd;            // Diffuse reflectivity
  vec3 Ks;            // Specular reflectivity
  float Shininess;    // Specular shininess factor
};

uniform MaterialInfo Material;
uniform float TexBlending;

void phongModel( vec3 pos, vec3 norm, out vec3 amb, out vec3 diff, out vec3 spec ) {
    vec3 s = normalize(vec3(Light.Position) - pos);
    vec3 v = normalize(-pos.xyz);
    vec3 r = reflect( -s, norm );
    amb = Light.Intensity * Material.Ka;
    float sDotN = max( dot(s,norm), 0.0 );
    diff = Light.Intensity * Material.Kd * sDotN;
    spec = vec3(0.0);
    if( sDotN > 0.0 )
        spec = Light.Intensity * Material.Ks *
               pow( max( dot(r,v), 0.0 ), Material.Shininess );
    //ambAndDiff = ambient + diffuse;
}

void main() {
   
 	float visibilidad = 1.0;
	float x, y;
	
	for (x = -0.5; x<= 0.5; x += 1.0 ) {
       	for (y = -0.5; y<= 0.5; y += 1.0 ) {
       		if (texture( shadowMap, ShadowCoord.xy + vec2(x,y)/700).z < ShadowCoord.z - 0.002)
       			visibilidad -= .4 / 4. ;
		}
    }
    
    vec3 amb, diff, spec;
    vec4 texColor = texture2D( Tex, TexCoord );

    phongModel( Position, Normal, amb, diff , spec );
	if (ShadowCoord.y < 1)
    	gl_FragColor = vec4( mix( amb + visibilidad * diff, visibilidad * texColor.rgb, TexBlending), 1.0) + vec4(visibilidad * spec,1.0);
	else
    	gl_FragColor = vec4( mix( amb + diff, texColor.rgb, TexBlending), 1.0) + vec4(spec,1.0);
}
