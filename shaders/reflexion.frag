#version 130

varying vec3 LightDir;
varying vec2 TexCoord;
varying vec3 ViewDir;
varying vec3 ReflectDir;  // The direction of the reflected ray
varying vec3 Normal;
varying vec4 ShadowCoord;

uniform sampler2D Tex;
uniform float TexBlending;

uniform sampler2D shadowMap;

struct LightInfo {
  vec4 Position;  // Light position in eye coords.
  vec3 Intensity; // A,D,S intensity
};

uniform LightInfo Light;
uniform float Transparencia;

struct MaterialInfo {
  vec3 Ka;            // Ambient reflectivity
  vec3 Ks;            // Specular reflectivity
  vec3 Kd;            // Difusa reflectivity
  float Shininess;    // Specular shininess factor
};

uniform MaterialInfo Material;

vec2 coordReflexion(vec3 reflDir){
	float x = reflDir.x;
	float y = reflDir.y;
	float z = reflDir.z;

	float latitud = 0;
	float azimut = 0;

	// latitud
	if (z > 0 ){
		latitud = atan( (sqrt( x * x + y * y)) / z );
	} else if (z < 0 ) {
		latitud = atan( (sqrt( x * x + y * y)) / z );
	} else {
		latitud = 1.5708; // PI / 2
	}

	// longitud
	if ( x > 0 && y > 0) {  // primer cuadrante
		azimut = atan( y / x );
	} else if ( x > 0 && y < 0) {  // cuarto cuadrante
		azimut = 6.2832 + atan( y / x );
	} else if ( x == 0 ) {
		// Busco el signo de y
		if ( y >= 0 ) azimut = 6.2832;
		else azimut = -6.2832;
	} else {  // x < 0, tercer y cuarto cuadrante
		azimut = 3.1416 + atan( y / x );
	}
	return vec2( azimut / 6.2832, - latitud / 3.1416);
}

vec3 phongModel( vec3 norm, vec3 diffR , vec3 lightDir) {
    float visibilidad = 1.0;
    if (texture(shadowMap, ShadowCoord.xy).z < ShadowCoord.z - 0.005) {
    	visibilidad = 0.5;
    }

    vec3 r = reflect( -lightDir, norm );
    vec3 ambient = Light.Intensity * Material.Ka;
    float sDotN = max( dot(lightDir, norm), 0.0 );
    vec3 diffuse = visibilidad * Light.Intensity * diffR * sDotN;

    vec3 spec = vec3(0.0);
    if( sDotN > 0.0 )
        spec = visibilidad * Light.Intensity * Material.Ks * pow( max( dot(r,ViewDir), 0.0 ), Material.Shininess );

    return ambient + diffuse + spec;
}

void main() {

	vec3 ColorFinal = phongModel( Normal, Material.Kd, LightDir);

	vec2 v = coordReflexion( ReflectDir );
	vec2 v2 = vec2(v.x * -1.0, v.y * -1.0);

	// buscar el color en el mapa de reflexion
    vec4 reflColor = texture(Tex, v2);

    gl_FragColor = vec4( mix(ColorFinal, reflColor.rgb , TexBlending), 1.0 );
   	gl_FragColor.a = Transparencia;

}
