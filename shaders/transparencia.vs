#version 130

in vec3 VertexPosition;
in vec3 VertexNormal;
in vec2 VertexTexCoord;

varying vec3 OutLightIntensity;

struct LightInfo {
	vec3 Intensity;
	vec3 La;
	vec3 Ls;
	vec4 Position;
};

uniform LightInfo Light;

struct MaterialInfo {
	vec3 Ka;
	vec3 Kd;
	vec3 Ks;
	float Shininess;
};

uniform MaterialInfo Material;

uniform float Transparencia;

uniform mat4 ModelMatrix;
uniform mat3 NormalMatrix;
uniform mat4 ProjectionMatrix;
uniform mat4 ViewMatrix;
uniform mat4 biasdepthMVP;

varying vec4 ShadowCoord;
uniform sampler2D Tex;

void getEyeSpace( out vec3 norm, out vec4 position )
{
    norm = normalize( NormalMatrix * VertexNormal);
    position = ViewMatrix * ModelMatrix * vec4(VertexPosition, 1.0);
}

vec3 phongModel( vec4 position, vec3 norm )
{
    vec3 s = normalize(vec3(Light.Position - position));
    vec3 v = normalize(-position.xyz);
    vec3 r = reflect( -s, norm );
    vec3 ambient = Light.La * Material.Ka;
    float sDotN = max( dot(s,norm), 0.0 );
    vec3 diffuse = Light.Intensity * Material.Kd * sDotN;
    vec3 spec = vec3(0.0);
    if( sDotN > 0.0 )
    spec = Light.Ls * Material.Ks *
    pow( max( dot(r,v), 0.0 ), Material.Shininess );
    return ambient + diffuse + spec;
}
void main()
{
	mat4 MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;
    vec3 eyeNorm;
    vec4 eyePosition;
    // Get the position and normal in eye space
    getEyeSpace(eyeNorm, eyePosition);
    // Evaluate the lighting equation.
    OutLightIntensity = phongModel( eyePosition, eyeNorm );
    gl_Position = MVP * vec4(VertexPosition, 1.0);
	ShadowCoord = biasdepthMVP * vec4(VertexPosition,1.0);
}
