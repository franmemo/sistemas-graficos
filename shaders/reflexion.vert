#version 130

in vec3 VertexPosition;
in vec3 VertexNormal;
in vec2 VertexTexCoord;
in vec4 VertexTangent;

struct LightInfo {
  vec4 Position;  // Light position in eye coords.
  vec3 Intensity; // A,D,S intensity
};

uniform LightInfo Light;

varying vec3 LightDir;
varying vec2 TexCoord;
varying vec3 ViewDir;
varying vec3 ReflectDir;  // The direction of the reflected ray
varying vec3 Normal;
varying vec4 ShadowCoord;

uniform vec3 WorldCameraPosition;

uniform mat4 ModelMatrix;
uniform mat4 ViewMatrix;
uniform mat3 NormalMatrix;
uniform mat4 ProjectionMatrix;
uniform mat4 biasdepthMVP;


void main()
{
	mat4 MV = ViewMatrix * ModelMatrix;
    // Transform normal and tangent to eye space
    vec3 norm = normalize(NormalMatrix * VertexNormal);
    vec3 tang = normalize(NormalMatrix * vec3(VertexTangent));

    // Compute the binormal
    vec3 binormal = normalize( cross( norm, tang ) ) * VertexTangent.w;

    // Matrix for transformation to tangent space
    mat3 toObjectLocal = mat3(
        tang.x, binormal.x, norm.x,
        tang.y, binormal.y, norm.y,
        tang.z, binormal.z, norm.z ) ;

    // Get the position in eye coordinates
    vec3 pos = vec3(MV * vec4(VertexPosition,1.0) );

    // Transform light dir. and view dir. to tangent space
    LightDir = normalize( toObjectLocal * ( Light.Position.xyz - pos) );
    ViewDir = toObjectLocal * normalize(-pos);

    // Pass along the texture coordinate
    TexCoord = VertexTexCoord;

	// Compute the reflected direction in world coords.
    vec3 worldPos = vec3( ModelMatrix * vec4(VertexPosition,1.0) );
    vec3 worldNorm = vec3( ModelMatrix * vec4(VertexNormal, 0.0) );
    vec3 worldView = normalize( WorldCameraPosition - worldPos );
    ReflectDir = reflect(-worldView, worldNorm );

	// Paso la normal al fragmento
	Normal = NormalMatrix * VertexNormal;

    gl_Position = ProjectionMatrix * MV * vec4(VertexPosition,1.0);
	ShadowCoord = biasdepthMVP * vec4(VertexPosition,1.0);
}
