#version 130

in vec3 VertexPosition;
in vec3 VertexNormal;
in vec2 VertexTexCoord;

varying vec3 Position;
varying vec3 Normal;
varying vec2 TexCoord;
varying vec4 ShadowCoord;

uniform mat4 ModelMatrix;
uniform mat3 NormalMatrix;
uniform mat4 ProjectionMatrix;
uniform mat4 ViewMatrix;
uniform mat4 biasdepthMVP;

void main()
{	
	mat4 MV = ViewMatrix * ModelMatrix;
    TexCoord = VertexTexCoord;
    Normal = normalize( NormalMatrix * VertexNormal);
    Position = vec3( MV * vec4(VertexPosition,1.0) );
	ShadowCoord = biasdepthMVP * vec4(VertexPosition, 1.0);	
    gl_Position = ProjectionMatrix * MV * vec4(VertexPosition,1.0);
}
