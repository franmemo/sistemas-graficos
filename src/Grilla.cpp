#include "Grilla.h"

Grilla::Grilla(int size) {
	size = 10;
	this->vertex_buffer_size = 12 * (2 * size + 1);
	this->vertex_buffer = new GLfloat[this->vertex_buffer_size];

	this->index_buffer_size = 4 * (2 * size + 1);
	this->index_buffer = new GLuint[this->index_buffer_size];

	int offset;
	for (int i = 0; i < size; i++) {
		offset = 24 * i;
		this->vertex_buffer[offset++] = -size;
		this->vertex_buffer[offset++] = i + 1;
		this->vertex_buffer[offset++] = 0;

		this->vertex_buffer[offset++] = size;
		this->vertex_buffer[offset++] = i + 1;
		this->vertex_buffer[offset++] = 0;

		this->vertex_buffer[offset++] = -size;
		this->vertex_buffer[offset++] = -(i + 1);
		this->vertex_buffer[offset++] = 0;

		this->vertex_buffer[offset++] = size;
		this->vertex_buffer[offset++] = -(i + 1);
		this->vertex_buffer[offset++] = 0;

		this->vertex_buffer[offset++] = i + 1;
		this->vertex_buffer[offset++] = -size;
		this->vertex_buffer[offset++] = 0;

		this->vertex_buffer[offset++] = i + 1;
		this->vertex_buffer[offset++] = size;
		this->vertex_buffer[offset++] = 0;

		this->vertex_buffer[offset++] = -(i + 1);
		this->vertex_buffer[offset++] = -size;
		this->vertex_buffer[offset++] = 0;

		this->vertex_buffer[offset++] = -(i + 1);
		this->vertex_buffer[offset++] = size;
		this->vertex_buffer[offset++] = 0;
	}

	offset = 24 * size;
	this->vertex_buffer[offset++] = -size;
	this->vertex_buffer[offset++] = 0;
	this->vertex_buffer[offset++] = 0;

	this->vertex_buffer[offset++] = size;
	this->vertex_buffer[offset++] = 0;
	this->vertex_buffer[offset++] = 0;

	this->vertex_buffer[offset++] = 0;
	this->vertex_buffer[offset++] = -size;
	this->vertex_buffer[offset++] = 0;

	this->vertex_buffer[offset++] = 0;
	this->vertex_buffer[offset++] = size;
	this->vertex_buffer[offset++] = 0;

	for (int i = 0; i < this->index_buffer_size; i++) {
		this->index_buffer[i] = i;
	}
}

Grilla::~Grilla(){}

void Grilla::render() {
	glEnableClientState(GL_VERTEX_ARRAY);

	glVertexPointer(3, GL_FLOAT, 0, this->vertex_buffer);

	glDrawElements(GL_LINES, this->index_buffer_size, GL_UNSIGNED_INT,
			this->index_buffer);

	glDisableClientState(GL_VERTEX_ARRAY);
}
