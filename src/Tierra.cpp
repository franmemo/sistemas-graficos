/*
 * Tierra.cpp
 *
 *  Created on: 10/08/2013
 *      Author: nacho
 */

#include "Tierra.h"
#include "FactoryMateriales.h"
#include "CacheShaders.h"

Tierra::Tierra(): suelo(10, 100.f) {
	defshader = shader = CacheShaders::getInstance().getShader(std::string("tierra"));
	material = FactoryMateriales::createTierra();
}

Tierra::~Tierra() {
	// TODO Auto-generated destructor stub
}


void Tierra::render(Luz* luz, Vista* view, glm::mat4 model_matrix) {
	shader->use();
	if (shader == defshader) {
		material->setMaterialProperties(shader);
		luz->setLightProperties(shader, view);
	}
	shader->setMatrixes(luz, view, glm::scale(model_matrix, glm::vec3(300.f)));
	this->suelo.render();
}
