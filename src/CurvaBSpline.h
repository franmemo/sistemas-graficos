#pragma once

#include "Curva.h"
#include <glm/glm.hpp>
#include <vector>

class CurvaBSpline :
	public Curva
{
private:
	glm::vec3 pact;
	glm::vec3 pant;
public:
	glm::vec3 tangente();
	CurvaBSpline(int pasos);
	CurvaBSpline(glm::vec3 origen, int pasos);
	CurvaBSpline(std::vector<glm::vec3> &puntos, int pasos);

	glm::vec3 siguiente();
	virtual ~CurvaBSpline(void);

	float b0(float u);
	float b1(float u);
	float b2(float u);
	float b3(float u);
};

