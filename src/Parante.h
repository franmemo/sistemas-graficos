#ifndef PARANTE_H_
#define PARANTE_H_
#include "Cilindro.h"
#include "ObjetoSimple.h"

class Parante: public ObjetoSimple {
public:
	Parante(unsigned int pisos);
	virtual ~Parante();
	void render(Luz* luz, Vista* view, glm::mat4 model_matrix);
private:
	int nivelDiscretizacion;
	Cilindro parante;
	unsigned int pisos;
};

#endif /* PARANTE_H_ */
