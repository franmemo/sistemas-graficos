#include "Superficie.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform2.hpp>
#include <glm/gtx/projection.hpp>
#include <glm/gtx/vector_angle.hpp>

#define DIR_PARCHE(x,y,z) ((x * y) + z)

Superficie::~Superficie(void)
{
}

void Superficie::render() {
	glBindVertexArray(vaoHandle);
	glDrawElements(GL_QUADS, this->index_buffer_size, GL_UNSIGNED_INT,
			this->index_buffer);

}

void Superficie::generarTriangulos(){
	std::vector<float> verticesBuffer;
	std::vector<float> normalesBuffer;
	std::vector<unsigned int> indexBuffer;
	std::vector<float> texCordsBuffer;
	std::vector<float> tangentsBuffer;

	for (int i = 0; i < puntos.size(); ++i){
		verticesBuffer.push_back(puntos[i].x);
		verticesBuffer.push_back(puntos[i].y);
		verticesBuffer.push_back(puntos[i].z);

		glm::vec3 t;
		if (i < puntos.size() -1) {
			t = puntos[i + 1] - puntos[i];
		} else {
			t = (puntos[i - 1] - puntos[i]) * 1.f;
		}

		tangentsBuffer.push_back(t.x);
		tangentsBuffer.push_back(t.y);
		tangentsBuffer.push_back(t.z);
		tangentsBuffer.push_back(1.0);

	}

	if (normales.size() == 0){
		for (int i = 0; i < puntos.size(); ++i){
			texCordsBuffer.push_back((i%alto)/(alto * 0.5f));
			texCordsBuffer.push_back(((int)(i/alto))/(alto * 0.5f));
		}
		//Calculo de normales estandar
		for (int i = 0; i < alto; ++i){
			for (int j=0; j < ancho; ++j){
				glm::vec3 normal, dir;
				if (j != ancho -1 && i != alto){
					dir = (puntos[i*ancho + j+1] - puntos[i*ancho + j]);
					normal = glm::cross(dir,(puntos[(i+1)*ancho + j] - puntos[i*ancho + j]));
				} else if (i != alto){
					dir = (puntos[i*ancho + j+1] - puntos[i*ancho + j]);
					normal = glm::cross(dir, (puntos[(i-1)*ancho + j]) - puntos[i*ancho + j]);
				} else if (j != ancho){
					dir = (puntos[i*ancho + j-1] - puntos[i*ancho + j]);
					normal = glm::cross(dir,(puntos[(i+1)*ancho + j] - puntos[i*ancho + j]));
				} else {
					dir = (puntos[i*ancho + j-1] - puntos[i*ancho + j]);
					normal = glm::cross(dir,puntos[i*ancho + j] - (puntos[(i-1)*ancho + j]));
				}
				
				normalesBuffer.push_back(normal.x);
				normalesBuffer.push_back(normal.y);
				normalesBuffer.push_back(normal.z);
/*
				glm::vec3 tan = glm::normalize(dir - n1 * glm::dot(normal, dir));

				tangentesBuffer.push_back(tan.x);
				tangentesBuffer.push_back(tan.y);
				tangentesBuffer.push_back(tan.z);
				tangentesBuffer.push_back(1);
*/
			}
		}
	} else {

		if (texturas.size() == 0){
			for (int i = 0; i < puntos.size() - (alto+1); ++i){
				int dis = 60;
				texCordsBuffer.push_back((i%dis)/(dis * 1.f));
				texCordsBuffer.push_back(((int)(i/dis))/(dis* 1.f));
			}
		} else {
			for (int i = 0; i < texturas.size(); ++i){
				texCordsBuffer.push_back(texturas[i].x * resolucion);
				texCordsBuffer.push_back(texturas[i].y * resolucion);
			}
/*
			for (int i = 0; i < puntos.size() - 5; ++i){
				int dis = 30;
				texCordsBuffer.push_back((i%5)/5.f);
				texCordsBuffer.push_back(((int)(i/5))/(5.f));
			}
*/		}
		for (int i = 0; i < normales.size(); ++i){
			normalesBuffer.push_back(normales[i].x);
			normalesBuffer.push_back(normales[i].y);
			normalesBuffer.push_back(normales[i].z);
		}
	}
	// Calculo de indices
	for (int i = 0; i < alto-1; ++i){
		for (int j = 0; j < ancho-1; ++j){
			indexBuffer.push_back(DIR_PARCHE(i,ancho,j));
			indexBuffer.push_back(DIR_PARCHE(i,ancho,(j+1)));
			indexBuffer.push_back(DIR_PARCHE((i+1),ancho,(j+1)));
			indexBuffer.push_back(DIR_PARCHE((i+1),ancho,j));
		}
	}

	//Paso los vectores a array[]
	this->vertex_buffer_size = verticesBuffer.size();
	this->vertex_buffer = new float [vertex_buffer_size];

	std::vector<float>::iterator it;
	unsigned int i = 0;
	for (it = verticesBuffer.begin(); it != verticesBuffer.end(); it++){
		this->vertex_buffer[i++] = (*it);
	}

	this->normal_buffer_size = normalesBuffer.size();
	this->normal_buffer = new float [normal_buffer_size];
	i = 0;
	for (it = normalesBuffer.begin(); it != normalesBuffer.end(); it++){
		this->normal_buffer[i++] = (*it);
	}

	this->index_buffer_size = indexBuffer.size();
	this->index_buffer = new unsigned int[index_buffer_size];
	std::vector<unsigned int>::iterator index_it;
	i = 0;
	for (index_it = indexBuffer.begin(); index_it != indexBuffer.end(); index_it++){
		this->index_buffer[i] = *index_it;
		i++;
	}

	this->texture_buffer_size = texCordsBuffer.size();
	this->texture_buffer = new float [texture_buffer_size];
	i=0;
	for (it = texCordsBuffer.begin(); it != texCordsBuffer.end(); it++){
		this->texture_buffer[i] = *it;
		i++;
	}

	this->tangent_buffer_size = tangentsBuffer.size();
	this->tangent_buffer = new float [tangent_buffer_size];
	i=0;
	for (it = tangentsBuffer.begin(); it != tangentsBuffer.end(); it++){
		this->tangent_buffer[i] = *it;
		i++;
	}



	glGenVertexArrays( 1, &vaoHandle );
	glBindVertexArray( vaoHandle );

	GLuint vboHandles[4];
	glGenBuffers(4, vboHandles);

	GLuint positionBufferHandle = vboHandles[0];
	GLuint normalBufferHandle = vboHandles[1];
	GLuint textureBufferHandle = vboHandles[2];
	GLuint tangenteBufferHandle = vboHandles[3];

	glBindBuffer( GL_ARRAY_BUFFER, positionBufferHandle );
	glBufferData( GL_ARRAY_BUFFER, vertex_buffer_size * sizeof (float), vertex_buffer, GL_STATIC_DRAW );
	glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, (GLubyte*)NULL);
	glEnableVertexAttribArray(0);

	glBindBuffer( GL_ARRAY_BUFFER, normalBufferHandle );
	glBufferData( GL_ARRAY_BUFFER, normal_buffer_size * sizeof (float), normal_buffer, GL_STATIC_DRAW );
	glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, 0, (GLubyte*)NULL);
	glEnableVertexAttribArray(1);

	glBindBuffer( GL_ARRAY_BUFFER, textureBufferHandle );
	glBufferData( GL_ARRAY_BUFFER, texture_buffer_size * sizeof (float), texture_buffer, GL_STATIC_DRAW );
	glVertexAttribPointer( 2, 2, GL_FLOAT, GL_FALSE, 0, (GLubyte*)NULL);
	glEnableVertexAttribArray(2);

	glBindBuffer( GL_ARRAY_BUFFER, tangenteBufferHandle );
	glBufferData( GL_ARRAY_BUFFER, tangent_buffer_size * sizeof(float), tangent_buffer, GL_STATIC_DRAW );
	glVertexAttribPointer( 3, 4, GL_FLOAT, GL_FALSE, 0, (GLubyte*)NULL);
	glEnableVertexAttribArray(3);

	glBindVertexArray(0);


}

Superficie::Superficie(unsigned int discretizacion, float resolucion): discretizacion(discretizacion), resolucion(resolucion){

}



