/*
 * ObjetoSimple.cpp
 *
 *  Created on: 14/08/2013
 *      Author: nacho
 */

#include "ObjetoSimple.h"

ObjetoSimple::ObjetoSimple() {
	material = NULL;
	defshader = shader = NULL;
}

ObjetoSimple::~ObjetoSimple() {

}

void ObjetoSimple::forceShader(Shader* sh) {
	if (sh == shader) return;
	defshader = shader;
	shader = sh;
}

void ObjetoSimple::restoreDefaultShader() {
	shader = defshader;
}
