/*
 * Textura.h
 *
 *  Created on: 06/08/2013
 *      Author: nacho
 */

#ifndef TEXTURA_H_
#define TEXTURA_H_

#include "Propiedad.h"
#include <string>

class Textura: public Propiedad {

static std::string base_path;

public:
	Textura(std::string textura, tipo_textura tipo, float texBlending = 0.3);
	virtual ~Textura();
	void applyPropiedad(Shader* shader, Material* material);

protected:
	std::string texture_id;
	tipo_textura tipo;
	GLuint tid;
	float texBlending;
	char* sampler;

};

#endif /* TEXTURA_H_ */
