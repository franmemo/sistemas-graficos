#include "Vista.h"

glm::vec4 Vista::luz(20.0, 20.0, 20.0, 1.f);

Vista::Vista(const glm::vec3 eye, const glm::vec3 at, const glm::vec3 up):
	eye(eye), at(at), up(up){
}

Vista::Vista(const Vista& v):
	eye(v.eye), at(v.at), up(v.up){
	projection_matrix = glm::infinitePerspective(52.0f, 4.f / 3.f, 0.1f);
}

glm::mat4 Vista::getViewMatrix(){
	return this->view_matrix;
}


glm::mat4 Vista::getProjectionMatrix(){
	return glm::infinitePerspective(52.0f, 4.f / 3.f, 0.1f);
}

Vista::~Vista(){}

glm::vec3 Vista::getEye(){
	return this->eye;
}

glm::vec3 Vista::getUp(){
	return this->up;
}

glm::vec3 Vista::getAt(){
	return this->at;
}



