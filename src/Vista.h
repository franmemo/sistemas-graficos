#ifndef __VISTA_H
#define __VISTA_H 

#pragma once 
#include <glm/glm.hpp> 
#include <glm/gtc/matrix_transform.hpp> 
#include <iostream>

class Vista{
public:
	glm::mat4 getViewMatrix();
	glm::mat4 getProjectionMatrix();

	virtual void moveMouse(int x, int y, int max_x, int max_y)=0;
	virtual void key(char cAscii, int max_x, int max_y)=0;

	Vista(const glm::vec3 eye, const glm::vec3 at, const glm::vec3 up);
	Vista(const Vista& v);
	virtual ~Vista() = 0;

	glm::vec3 getEye();
	glm::vec3 getUp();
	glm::vec3 getAt();

	static glm::vec4 luz;
protected:
	glm::vec3 eye, up, at; 
	glm::mat4 view_matrix;
	glm::mat4 projection_matrix;

};

#endif /* __VISTA_H  */
