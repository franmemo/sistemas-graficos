#include "Esfera.h"

#include <glm/glm.hpp> 
#include <glm/gtc/matrix_transform.hpp> 
#include <glm/gtx/transform2.hpp> 
#include <glm/gtx/projection.hpp>

#include <vector>

#define CARAS 32

void Esfera::crearCara(float latitud, float azimut){
	float x, y, z;
	// primer punto
	x = sin(latitud) * cos(azimut);
	y = sin(latitud) * sin(azimut);
	z = cos(latitud);
	// agregarlos al buffer
	verticesBuffer.push_back(x);
	verticesBuffer.push_back(y);
	verticesBuffer.push_back(z);
	// coordenadas de textura
	texCordsBuffer.push_back(azimut / ( 2 * PI ));
	texCordsBuffer.push_back(latitud / PI);
	//segundo punto ("abajo")
	float x2 = sin(latitud + PI/CARAS) * cos(azimut);
	float y2 = sin(latitud + PI/CARAS) * sin(azimut);
	float z2 = cos(latitud + PI/CARAS);
	// agregarlos al buffer
	verticesBuffer.push_back(x2);
	verticesBuffer.push_back(y2);
	verticesBuffer.push_back(z2);
	// coordenadas de textura
	texCordsBuffer.push_back(azimut / ( 2 * PI ));
	texCordsBuffer.push_back((latitud + PI/CARAS) / PI);
	// Tercer punto ("abajo a la derecha")
	float x3 = sin(latitud + PI/CARAS) * cos(azimut + ( 2.0f * PI )/CARAS);
	float y3 = sin(latitud + PI/CARAS) * sin(azimut + ( 2.0f * PI )/CARAS);
	float z3 = cos(latitud + PI/CARAS);
	// agregarlos al buffer
	verticesBuffer.push_back(x3);
	verticesBuffer.push_back(y3);
	verticesBuffer.push_back(z3);
	// coordenadas de textura
	texCordsBuffer.push_back((azimut + ( 2.0f * PI )/CARAS) / ( 2 * PI ));
	texCordsBuffer.push_back((latitud + PI/CARAS) / PI);
	// cuarto punto ("derecha")
	float x4 = sin(latitud ) * cos(azimut + ( 2 * PI )/CARAS);
	float y4 = sin(latitud ) * sin(azimut + ( 2 * PI )/CARAS);
	float z4 = cos(latitud );
	// agregarlos al buffer
	verticesBuffer.push_back(x4);
	verticesBuffer.push_back(y4);
	verticesBuffer.push_back(z4);
	// coordenadas de textura
	texCordsBuffer.push_back((azimut + ( 2 * PI )/CARAS) / ( 2 * PI ));
	texCordsBuffer.push_back(latitud / PI);
	
	// tangente del primero
	glm::vec3 t;
	glm::vec3 n1(x, y, z);
	if (latitud != 0) {
		t = glm::vec3 (x4 - x, y4 - y, z4 - z);
		t = glm::normalize(t - n1 * glm::dot(n1, t));
		tangentesBuffer.push_back(t.x);
		tangentesBuffer.push_back(t.y);
		tangentesBuffer.push_back(t.z);
		tangentesBuffer.push_back(1);
	} else {
		// caso particular del polo norte
		t = glm::vec3 (x3 - x2, y3 - y2, z3 - z2);
		t = glm::normalize(t - n1 * glm::dot(n1, t));
		tangentesBuffer.push_back(t.x);
		tangentesBuffer.push_back(t.y);
		tangentesBuffer.push_back(t.z);
		tangentesBuffer.push_back(1);
	}
	
	// tangente del segundo
	glm::vec3 n2(x2, y2, z2);
	if (latitud != PI) {
		t = glm::vec3 (x3 - x2, y3 - y2, z3 - z2);
		t = glm::normalize(t - n2 * glm::dot(n2, t));
		tangentesBuffer.push_back(t.x);
		tangentesBuffer.push_back(t.y);
		tangentesBuffer.push_back(t.z);
		tangentesBuffer.push_back(1);
	} else {
		// Caso particular del polo sur
		t = glm::vec3 (x4 - x, y4 - y, z4 - z);
		t = glm::normalize(t - n2 * glm::dot(n2, t));
		tangentesBuffer.push_back(t.x);
		tangentesBuffer.push_back(t.y);
		tangentesBuffer.push_back(t.z);
		tangentesBuffer.push_back(1);
	}
	
	// tangente del tercero
	glm::vec3 n3(x3, y3, z3);
	if (latitud != PI) {
		float x3d = sin(latitud + PI/CARAS) * cos(azimut + 2.0f*( 2 * PI )/CARAS);
		float y3d = sin(latitud + PI/CARAS) * sin(azimut + 2.0f*( 2 * PI )/CARAS);
		float z3d = cos(latitud + PI/CARAS);
		t = glm::vec3 (x3d - x3, y3d - y3, z3d - z3);
		t = glm::normalize(t - n3 * glm::dot(n3, t));
		tangentesBuffer.push_back(t.x);
		tangentesBuffer.push_back(t.y);
		tangentesBuffer.push_back(t.z);	
		tangentesBuffer.push_back(1);
	} else {
		// Caso particular del polo sur
		t = glm::vec3 (x4 - x, y4 - y, z4 - z);
		t = glm::normalize(t - n3 * glm::dot(n3, t));
		tangentesBuffer.push_back(t.x);
		tangentesBuffer.push_back(t.y);
		tangentesBuffer.push_back(t.z);	
		tangentesBuffer.push_back(1);
	}
	
	// tangente del cuarto
	glm::vec3 n4(x4,y4,z4);
	if (latitud != 0) {
		float x4d = sin(latitud ) * cos(azimut + 2.0f*( 2 * PI )/CARAS);
		float y4d = sin(latitud ) * sin(azimut + 2.0f*( 2 * PI )/CARAS);
		float z4d = cos(latitud );
		t = glm::vec3 (x4d - x4, y4d - y4, z4d - z4);
		t = glm::normalize(t - n4 * glm::dot(n4, t));
		tangentesBuffer.push_back(t.x);
		tangentesBuffer.push_back(t.y);
		tangentesBuffer.push_back(t.z);
		tangentesBuffer.push_back(1);
	} else {
		// caso particular del polo norte
		t = glm::vec3 (x3 - x2, y3 - y2, z3 - z2);
		t = glm::normalize(t - n4 * glm::dot(n4, t));
		tangentesBuffer.push_back(t.x);
		tangentesBuffer.push_back(t.y);
		tangentesBuffer.push_back(t.z);
		tangentesBuffer.push_back(1);
	}
}

void Esfera::createSpiralSphere()
{
 	float latitud, azimut;
	for (int i = 0; i < CARAS; ++i){
		latitud = i * PI / CARAS;
		for (int j = 0; j < CARAS; ++j){
			azimut = j * (2 * PI) / CARAS;
			crearCara(latitud, azimut);
		}
	}

	// copiar de los vectores a los punteros
	texture_buffer_size = this->texCordsBuffer.size();
    this->texture_buffer = new float [texture_buffer_size];
	
    std::vector<float>::iterator text_it;
    int pos = 0;
    for (text_it = this->texCordsBuffer.begin(); text_it != this->texCordsBuffer.end(); text_it++)
    {
        this->texture_buffer[pos] = *text_it;
        pos++;
    }

    vertex_buffer_size = this->verticesBuffer.size();
    this->vertex_buffer = new float [vertex_buffer_size];
	pos = 0;
    for (text_it = this->verticesBuffer.begin(); text_it != this->verticesBuffer.end(); text_it++)
    {
        this->vertex_buffer[pos] = *text_it;
        pos++;
    }
	
    this->tangent_buffer_size = tangentesBuffer.size();
    this->tangent_buffer = new float [this->tangentesBuffer.size()];
	pos = 0;
    for (text_it = this->tangentesBuffer.begin(); text_it != this->tangentesBuffer.end(); text_it++)
    {
        this->tangent_buffer[pos] = *text_it;
        pos++;
    }
}

void Esfera::render() {
	glBindVertexArray( vaoHandle );
	glDrawArrays(GL_QUADS, 0, 4 * CARAS * CARAS);

}

Esfera::Esfera()
{
    createSpiralSphere();
    glGenVertexArrays( 1, &vaoHandle );
    glBindVertexArray( vaoHandle );
    GLuint vboHandles[4];
    glGenBuffers(4, vboHandles);

    GLuint positionBufferHandle = vboHandles[0];
    GLuint normalBufferHandle = vboHandles[1];
    GLuint textureHandler = vboHandles[2];
    GLuint tangenteHandler = vboHandles[3];

    glBindBuffer( GL_ARRAY_BUFFER, positionBufferHandle );
    glBufferData( GL_ARRAY_BUFFER, vertex_buffer_size * sizeof (float), vertex_buffer, GL_STATIC_DRAW );
    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, (GLubyte*)NULL);
    glEnableVertexAttribArray(0);

    glBindBuffer( GL_ARRAY_BUFFER, normalBufferHandle );
    glBufferData( GL_ARRAY_BUFFER, vertex_buffer_size * sizeof (float), vertex_buffer, GL_STATIC_DRAW );
    glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, 0, (GLubyte*)NULL);
    glEnableVertexAttribArray(1);

    glBindBuffer( GL_ARRAY_BUFFER, textureHandler );
    glBufferData( GL_ARRAY_BUFFER, texture_buffer_size * sizeof (float), texture_buffer, GL_STATIC_DRAW );
    glVertexAttribPointer( 2, 2, GL_FLOAT, GL_FALSE, 0, (GLubyte*)NULL);
    glEnableVertexAttribArray(2);

    glBindBuffer( GL_ARRAY_BUFFER, tangenteHandler );
    glBufferData( GL_ARRAY_BUFFER, tangent_buffer_size * sizeof (float), tangent_buffer, GL_STATIC_DRAW );
    glVertexAttribPointer( 3, 4, GL_FLOAT, GL_FALSE, 0, (GLubyte*)NULL);
    glEnableVertexAttribArray(3);
	glBindVertexArray(0);
}

