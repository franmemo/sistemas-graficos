/*
 * Opacidad.h
 *
 *  Created on: 06/08/2013
 *      Author: nacho
 */

#ifndef OPACIDAD_H_
#define OPACIDAD_H_

#include "Propiedad.h"

class Opacidad: public Propiedad {
public:
	Opacidad(float grado);
	virtual ~Opacidad();
	void applyPropiedad(Shader* shader, Material* material);

private:
	float grado;
};

#endif /* OPACIDAD_H_ */
