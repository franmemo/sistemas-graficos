#ifndef __Objeto_H
#define __Objeto_H

#pragma once

#include <glm/glm.hpp> 
#include <glm/gtc/matrix_transform.hpp> 
#include "Definiciones.h"

class Shader;
class Luz;
class Vista;

class Objeto {
public:
	virtual void render(Luz* luz, Vista* view, glm::mat4 model_matrix) = 0;
	virtual void forceShader(Shader* shader) = 0;
	virtual void restoreDefaultShader() = 0;
	virtual ~Objeto();
};

#endif /* __OBJETO_H */
