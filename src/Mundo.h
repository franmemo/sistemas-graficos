/*
 * Mundo.h
 *
 *  Created on: 06/07/2013
 *      Author: nacho
 */

#ifndef MUNDO_H_
#define MUNDO_H_

#include "ObjetoCompuesto.h"


class Mundo: public ObjetoCompuesto {
public:
	void render(Luz* luz, Vista* view, glm::mat4 model_matrix);
	Mundo();
	virtual ~Mundo();
};

#endif /* MUNDO_H_ */
