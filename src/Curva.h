#pragma once

#include <vector>
#include <glm/glm.hpp> 

class Curva{
public:
	Curva(int pasos);
	Curva(glm::vec3 origen, int pasos);
	Curva(std::vector<glm::vec3> &puntos, int pasos);
	virtual ~Curva();
	void agregarPunto(glm::vec3 s);
	std::vector<glm::vec3> vector();

	//Funciones para iterar
	virtual glm::vec3 siguiente() = 0;
	bool tieneSiguiente();
protected:
	int pasos;
	int p0Actual;  
	std::vector<glm::vec3> puntos;
	float u;

	virtual float b0(float u)=0;
	virtual float b1(float u)=0;
	virtual float b2(float u)=0;
	virtual float b3(float u)=0;
	
};

