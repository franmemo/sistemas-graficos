/*
 * MatrixSetterRender.cpp
 *
 *  Created on: 14/08/2013
 *      Author: nacho
 */

#include "MatrixSetterRender.h"

#include <GL/glew.h>
#include <GL/freeglut.h>

MatrixSetterRender::MatrixSetterRender() {
	// TODO Auto-generated constructor stub

}

MatrixSetterRender::~MatrixSetterRender() {
	// TODO Auto-generated destructor stub
}

void MatrixSetterRender::setMatrixes(PropertySetter* shader, Luz* luz, Vista* vista, glm::mat4 model_matrix) {
	glm::mat4 view = vista->getViewMatrix();
	glm::mat4 projection = vista->getProjectionMatrix();
	glm::mat4 mv = view * model_matrix;

	shader->setUniform("ModelMatrix", model_matrix);
	shader->setUniform("ViewMatrix", view);
	shader->setUniform("NormalMatrix",
			glm::mat3( glm::vec3(mv[0]), glm::vec3(mv[1]), glm::vec3(mv[2]) ));
	shader->setUniform("ProjectionMatrix", projection);

	glm::mat4 v2 = luz->getViewMatrix();
	glm::mat4 p2 = luz->getProjectionMatrix();

	glm::mat4 biasMatrix(
	0.5, 0.0, 0.0, 0.0,
	0.0, 0.5, 0.0, 0.0,
	0.0, 0.0, 0.5, 0.0,
	0.5, 0.5, 0.5, 1.0
	);
	shader->setUniform("biasdepthMVP", biasMatrix * p2 * v2 * model_matrix);
	shader->setUniform("shadowMap", SHADOWMAP);
}
