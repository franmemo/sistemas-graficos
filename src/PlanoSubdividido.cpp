#include "PlanoSubdividido.h"

#define PL_PI 3.141592653589793


PlanoSubdividido::PlanoSubdividido(int segmentosPorArista, float resolucion):
		segmentosPorArista(segmentosPorArista)  {

	this->vertex_buffer_size = (segmentosPorArista + 1) * (segmentosPorArista + 1 ) * 3;
	this->vertex_buffer = new GLfloat[this->vertex_buffer_size];

	this->normal_buffer_size = (segmentosPorArista + 1) * (segmentosPorArista + 1 ) * 3;
	this->normal_buffer = new GLfloat[this->normal_buffer_size];


	this->index_buffer_size = 4 * segmentosPorArista * segmentosPorArista;
	this->index_buffer = new GLuint[this->index_buffer_size];

	this->texture_buffer_size = 2 * (segmentosPorArista + 1) * (segmentosPorArista + 1);
	this->texture_buffer = new GLfloat[this->texture_buffer_size];

	this->tangent_buffer_size = 4 * (segmentosPorArista + 1) * (segmentosPorArista + 1);
	this->tangent_buffer = new GLfloat[this->tangent_buffer_size];

	int seg = segmentosPorArista + 1;
	float unitsSize = 2. / segmentosPorArista;
	for (int i = 0; i < seg; i++) {
		for (int j = 0; j < seg; j++) {
			this->vertex_buffer[i * seg * 3 + 3 * j] = i * unitsSize - 1;	//Vx
			this->vertex_buffer[i * seg * 3 + 3 * j + 1] = j * unitsSize - 1;	//Vy
			this->vertex_buffer[i * seg * 3 + 3 * j + 2] = 0;	//Vz

			this->normal_buffer[i * seg * 3 + 3 * j] = 0;	//Vx
			this->normal_buffer[i * seg * 3 + 3 * j + 1] = 0;	//Vy
			this->normal_buffer[i * seg * 3 + 3 * j + 2] = 1;	//Vz

			this->texture_buffer[i * seg * 2 + 2 * j ] = (i % 2) * resolucion;
			this->texture_buffer[i * seg * 2 + 2 * j + 1] = (j % 2) * resolucion;

			this->tangent_buffer[i * seg * 4 + 4 * j] = 0;
			this->tangent_buffer[i * seg * 4 + 4 * j + 1] = 1;
			this->tangent_buffer[i * seg * 4 + 4 * j + 2] = 0;
			this->tangent_buffer[i * seg * 4 + 4 * j + 3] = 1;

		}
	}
	unsigned int k = 0;
	for (int i = 0; i < segmentosPorArista; i++) {
		for (int j = 0; j < segmentosPorArista ; j++) {
			this->index_buffer[k++] = i * seg  + j;
			this->index_buffer[k++] = (i+1) * seg + j;
			this->index_buffer[k++] = (i+1) * seg + j + 1;
			this->index_buffer[k++] = i * seg + j + 1;
		}
	}

	glGenVertexArrays( 1, &vaoHandle );
	glBindVertexArray( vaoHandle );

	GLuint vboHandles[4];
	glGenBuffers(4, vboHandles);

	GLuint positionBufferHandle = vboHandles[0];
	GLuint normalBufferHandle = vboHandles[1];
	GLuint textureBufferHandle = vboHandles[2];
	GLuint tangenteBufferHandle = vboHandles[3];

	glBindBuffer( GL_ARRAY_BUFFER, positionBufferHandle );
	glBufferData( GL_ARRAY_BUFFER, vertex_buffer_size * sizeof (float), vertex_buffer, GL_STATIC_DRAW );
	glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, (GLubyte*)NULL);
	glEnableVertexAttribArray(0);

	glBindBuffer( GL_ARRAY_BUFFER, normalBufferHandle );
	glBufferData( GL_ARRAY_BUFFER, normal_buffer_size * sizeof (float), normal_buffer, GL_STATIC_DRAW );
	glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, 0, (GLubyte*)NULL);
	glEnableVertexAttribArray(1);

	glBindBuffer( GL_ARRAY_BUFFER, textureBufferHandle );
	glBufferData( GL_ARRAY_BUFFER, texture_buffer_size * sizeof (float), texture_buffer, GL_STATIC_DRAW );
	glVertexAttribPointer( 2, 2, GL_FLOAT, GL_FALSE, 0, (GLubyte*)NULL);
	glEnableVertexAttribArray(2);

	glBindBuffer( GL_ARRAY_BUFFER, tangenteBufferHandle );
	glBufferData( GL_ARRAY_BUFFER, tangent_buffer_size * sizeof(float), tangent_buffer, GL_STATIC_DRAW );
	glVertexAttribPointer( 3, 4, GL_FLOAT, GL_FALSE, 0, (GLubyte*)NULL);
	glEnableVertexAttribArray(3);

	glBindVertexArray(0);
}
PlanoSubdividido::~PlanoSubdividido(){
	if (this->normal_buffer != NULL) {
		delete[] this->normal_buffer;
	}
}
void PlanoSubdividido::render() {
	glBindVertexArray( vaoHandle );
	glDrawElements(GL_QUADS, this->index_buffer_size, GL_UNSIGNED_INT,
				this->index_buffer);}

