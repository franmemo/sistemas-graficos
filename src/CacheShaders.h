/*
 * CacheShaders.h
 *
 *  Created on: 10/08/2013
 *      Author: nacho
 */

#ifndef CACHESHADERS_H_
#define CACHESHADERS_H_
#include "Shader.h"
#include <map>
#include <string>
#include <vector>

class CacheShaders {
public:
	static CacheShaders* instance;
	static CacheShaders& getInstance();
	Shader* getShader(std::string object_id);
	static void close();
	bool isActive(Shader* shader);
	void activate(Shader* shader);
	static void initShaders();
private:
	std::map<std::string, Shader*> shadersMap;
	CacheShaders();
	virtual ~CacheShaders();
	Shader* active;
	Shader* shref, *shtex, *shnorm, *shruido;
};

#endif /* CACHESHADERS_H_ */
