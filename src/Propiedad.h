/*
 * Propiedad.h
 *
 *  Created on: 06/08/2013
 *      Author: nacho
 */

#ifndef PROPIEDAD_H_
#define PROPIEDAD_H_

#include "Shader.h"
#include "Definiciones.h"

class Material;

class Propiedad {
public:
	Propiedad();
	virtual ~Propiedad();
	virtual void applyPropiedad(Shader* shader, Material* material) = 0;
};

#endif /* PROPIEDAD_H_ */

