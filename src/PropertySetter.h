#ifndef PROPERTYSETTER_H
#define PROPERTYSETTER_H

#include <glm/glm.hpp>

class PropertySetter {
public:
	virtual void setUniform(const char* name,const glm::mat4& matrix) = 0;
	virtual void setUniform(const char* name,const glm::mat3& matrix) = 0;
	virtual void setUniform(const char* name,const glm::vec3& vector) = 0;
	virtual void setUniform(const char* name,const glm::vec4& vector) = 0;
	virtual void setUniform(const char* name,const glm::vec2& vector) = 0;
	virtual void setUniform(const char* name, bool value) = 0;
	virtual void setUniform(const char* name, int value) = 0;
	virtual void setUniform(const char* name, float value) = 0;
	virtual ~PropertySetter();
};

#endif
