#ifndef __DEFINICIONES_H
#define __DEFINICIONES_H

#pragma once

#define PI 3.14159265358979f

#define GRUA_UNIDAD_LADO 1.4f

#define RAD(x) (x * PI / 180)


#define BASE_FILE_SHADER "shaders/"

enum tipo_monituclo {
	MONTICULO_ARENA = 0, MONTICULO_PIEDRA
};

enum tipo_textura {
	TEXTURA = 0, NORMALMAP, SHADOWMAP
};

#endif
