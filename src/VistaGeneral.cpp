#include "VistaGeneral.h"

void VistaGeneral::moveMouse(int x, int y, int max_x, int max_y){
	float mid_windowWidth = max_x / 2.f;
	float mid_windowHeight = max_y / 2.f;
	float tAngXY = angXY, tAngXZ= angXZ;
	if (x > mid_windowWidth)
		tAngXY += 0.05f * ((x - mid_windowWidth) / mid_windowWidth);
	else
		tAngXY -= 0.05f * ((mid_windowWidth - x) / mid_windowWidth);

	if (tAngXZ > -1.37 && y > mid_windowHeight)
		tAngXZ -= 0.05f * ((y - mid_windowHeight) / mid_windowHeight);
	else if (angXZ < 1.37 && y < mid_windowHeight)
		tAngXZ += 0.05f * ((mid_windowHeight - y) / mid_windowHeight);

	glm::vec3 v = (glm::vec3(eyeMod * std::cos(tAngXZ) * std::cos(tAngXY), 
								eyeMod * std::cos(tAngXZ) * std::sin(tAngXY), 
										eyeMod * std::sin(tAngXZ)))+this->at;
	if (v.z >= 0.5f){
		this->angXZ = tAngXZ;
	} else {
		v = (glm::vec3(eyeMod * std::cos(this->angXZ) * std::cos(tAngXY), 
									eyeMod * std::cos(this->angXZ) * std::sin(tAngXY), 
											eyeMod * std::sin(this->angXZ)))+this->at;
	}
	this->eye = v;
	this->angXY = tAngXY;
	view_matrix = glm::lookAt(this->getEye(), this->getAt(), this->getUp());
}

void VistaGeneral::key(char cAscii, int max_x, int max_y){
	float tEyeMod = this->eyeMod;

	switch(cAscii){
		case '+':
			if( tEyeMod > 1.)
				tEyeMod -= 1.;
			break;
		case '-':
			tEyeMod += 1.;
			break;
		default:
			break;	
	}
	glm::vec3 v = (glm::vec3(tEyeMod * std::cos(angXZ) * std::cos(angXY), 
								tEyeMod * std::cos(angXZ) * std::sin(angXY), 
										tEyeMod * std::sin(angXZ)))+this->at;
	if (v.z >= 0){
		this->eyeMod = tEyeMod;
		this->eye = v;
	}
	view_matrix = glm::lookAt(this->getEye(), this->getAt(), this->getUp());
}


VistaGeneral::VistaGeneral(float eyeMod,float angXY,float angXZ, const glm::vec3 at):
	Vista(glm::vec3(), at, glm::vec3(0.,0.,1.)), 
	eyeMod(eyeMod), 
	angXY(angXY), 
	angXZ(angXZ){

	this->eye = glm::vec3(eyeMod * std::cos(angXZ) * std::cos(angXY), 
								eyeMod * std::cos(angXZ) * std::sin(angXY), 
										eyeMod * std::sin(angXZ)) + at;

	view_matrix = glm::lookAt(this->getEye(), this->getAt(), this->getUp());
}

VistaGeneral::~VistaGeneral(){

}

