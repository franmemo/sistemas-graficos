/*
 * Luz.cpp
 *
 *  Created on: 10/08/2013
 *      Author: nacho
 */

#include "Luz.h"

void Luz::setLightProperties(PropertySetter* shader, Vista* vista) {
	shader->setUniform("Light.Position", /*vista->getViewMatrix() **/ position);
	shader->setUniform("Light.Intensity", intensity);
}

void Luz::setPosition(glm::vec4& position) {
	this->position = position;
}

glm::vec4& Luz::getPosition() {
	return position;
}

glm::mat4 Luz::getViewMatrix() {
	return view_matrix;
}

glm::mat4 Luz::getProjectionMatrix() {
	return projection_matrix;
}

Luz::Luz() {
	position = glm::vec4(20.0f, 20.0f, 20.0f, 1.0);
	intensity = glm::vec3(0.7);
	view_matrix = glm::lookAt(glm::vec3(position), glm::vec3(-position), glm::vec3(0,0,1));
	projection_matrix = glm::mat4 ( 1.0f );
	projection_matrix = glm::infinitePerspective( 52.0f , (float)640 / (float)369, 0.1f);
}

Luz::~Luz() {
}

Luz::Luz(glm::vec4& posicion, glm::vec3& intensity): position(posicion), intensity(intensity) {
	view_matrix = glm::lookAt(glm::vec3(position), glm::vec3(-position), glm::vec3(0,0,1));
	projection_matrix = glm::mat4 ( 1.0f );
	projection_matrix = glm::infinitePerspective( 52.0f , (float)640 / (float)369, 0.1f);

}
