#include "SuperficieRevolucion.h"
#include "CurvaBezier.h"
#include <glm/gtc/matrix_transform.hpp> 
#include <glm/gtx/transform2.hpp> 
#include <glm/gtx/projection.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <iostream>
SuperficieRevolucion::~SuperficieRevolucion(void)
{
}

SuperficieRevolucion::SuperficieRevolucion(Curva& c, unsigned int discretizacion, float resolucion):
	Superficie(discretizacion, resolucion),
	c(c){
	generarMalla();
	generarTriangulos();	
}

void SuperficieRevolucion::generarMalla(){
	float paso = 2 * PI / discretizacion, u, v;
	std::vector<glm::vec3> curva = c.vector();
	//Calculo los puntos en base a la rotacion de la curva
	float phi = 0.f;
	int i = 0;
	while(phi< 2*PI + 0.5f * paso) {
		float senPhi = std::sin(phi);
		float cosPhi = std::cos(phi);
		std::vector<glm::vec3>::iterator it;
		float dy = 1.f/curva.size();
		float y = 0;
		for (it = curva.begin(); it != curva.end(); it++){
			glm::vec3 p(*it);
			p.x = (*it).x * cosPhi + (*it).y * senPhi;
			p.y = (*it).y * cosPhi - (*it).x * senPhi; 
			puntos.push_back(p);

			// coordenadas de textura
			texturas.push_back(glm::vec2(phi / (2 * PI),y));
			y += dy;

		}
		phi += paso;
	}

	alto = puntos.size() / curva.size();
	ancho = curva.size();

	for (int i = 0; i < alto-1; ++i){
		for (int j=0; j < ancho; ++j){			
			// normales
			normales.push_back(glm::cross((puntos[(i+1)*ancho + j] - puntos[i*ancho + j]), (puntos[i*ancho + j+1] - puntos[i*ancho + j])));
		}
	}
	for (int j=0; j < ancho; ++j){			
		// normales
		normales.push_back(glm::cross((puntos[ancho + j] - puntos[j]),(puntos[j+1] - puntos[j])));
	}


}

glm::vec3 SuperficieRevolucion::evaluar(float u, float v){
	return glm::vec3();
}


