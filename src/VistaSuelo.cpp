#include "VistaSuelo.h"
#include "Definiciones.h"

void VistaSuelo::moveMouse(int x, int y, int max_x, int max_y){
	float mid_windowWidth = max_x / 2.f;
	float mid_windowHeight = max_y / 2.f;
	glm::vec3 direction_versor = glm::normalize(glm::cross(this->up,this->eye - this->at));
	glm::vec3 up_versor = glm::normalize(this->up);
	if (x > mid_windowWidth * 3 / 2){
		this->at += 0.2f * direction_versor;
	}else if (x < mid_windowWidth / 2){
		this->at -= 0.2f * direction_versor;
	}
	float ang = std::acos(glm::dot(up_versor, direction_versor));
	if (ang > 0.1 && ang < PI){
		if (y > mid_windowHeight * 3 / 2){
			this->at -= 0.2f * up_versor;
		}else if (y < mid_windowHeight/2){
			this->at += 0.2f * up_versor;
		}
	}
	view_matrix = glm::lookAt(this->getEye(), this->getAt(), this->getUp());
}

void VistaSuelo::key(char cAscii, int max_x, int max_y){
	glm::vec3 versor = glm::normalize(this->eye - this->at);
	versor.z = 0.;
	glm::vec3 ortho_versor = glm::normalize(glm::cross(this->up,versor));
	ortho_versor.z = 0.;
	switch(cAscii){
		case 'a':
			this->at -= ortho_versor;
			this->eye -= ortho_versor;
			break;
		case 's':
			this->at += versor;
			this->eye += versor;
			break;
		case 'd':
			this->at += ortho_versor;
			this->eye += ortho_versor;
			break;
		case 'w':
			this->at -= versor;
			this->eye -= versor;
			break;
		case 'x':
			this->at += glm::vec3(0.f,0.f,0.1f);
			this->eye +=  glm::vec3(0.f,0.f,0.1f);
			break;
		case 'c':
			if (at.z > 0.1f){
				this->at -= glm::vec3(0.f,0.f,0.1f);
				this->eye -=  glm::vec3(0.f,0.f,0.1f);
			}
			break;
		default:
			break;
	}
	view_matrix = glm::lookAt(this->getEye(), this->getAt(), this->getUp());
}

VistaSuelo::VistaSuelo(const glm::vec3 eye, const glm::vec3 at, const glm::vec3 up):
	Vista(eye, at, up){
	view_matrix = glm::lookAt(this->getEye(), this->getAt(), this->getUp());
}

VistaSuelo::~VistaSuelo(){

}

