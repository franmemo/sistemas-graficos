/*
 * MatrixSetterShadow.cpp
 *
 *  Created on: 14/08/2013
 *      Author: nacho
 */

#include "MatrixSetterShadow.h"

MatrixSetterShadow::MatrixSetterShadow() {
	// TODO Auto-generated constructor stub

}

MatrixSetterShadow::~MatrixSetterShadow() {
	// TODO Auto-generated destructor stub
}

void MatrixSetterShadow::setMatrixes(PropertySetter* ps, Luz* luz, Vista* vista, glm::mat4 model_matrix) {
	glm::mat4 depthMatrix = luz->getProjectionMatrix() * luz->getViewMatrix() * model_matrix;
	ps->setUniform("MVP", depthMatrix);
}
