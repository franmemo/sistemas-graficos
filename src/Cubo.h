#ifndef __CUBO_H
#define __CUBO_H 

#include "Primitiva.h"

class Cubo: public Primitiva {
public:
	Cubo(float resolucion = 1.0f);
	~Cubo();
	virtual void render();
};

#endif /* __CUBO_H */
