#ifndef COLUMNA_H
#define COLUMNA_H

#include "Cilindro.h"
#include "ObjetoSimple.h"

class Columna : public ObjetoSimple
{
    public:
        Columna(unsigned int pisos);
        virtual ~Columna();
    	void render(Luz *luz, Vista* view, glm::mat4 model_matrix);
    private:
        Cilindro cilindro;
};

#endif // COLUMNA_H
