#include "ShaderNormales.h"
#include "MatrixSetterRender.h"

ShaderNormales::ShaderNormales()
{
    glShadeModel(GL_SMOOTH);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_POLYGON_SMOOTH);
	compileShaderVertex("shaders/normalmap.vert");
	compileShaderFragment("shaders/normalmap.frag");
	glBindAttribLocation(handle, 0, "VertexPosition");
    glBindAttribLocation(handle, 1, "VertexNormal");
	glBindAttribLocation(handle, 2, "VertexTexCoord");
	glBindAttribLocation(handle, 3, "VertexTangent");
	ms = new MatrixSetterRender();
}

ShaderNormales::~ShaderNormales()
{
	delete ms;
}
