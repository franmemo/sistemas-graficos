/*
 * Piso.h
 *
 *  Created on: 10/08/2013
 *      Author: nacho
 */

#ifndef PISO_H_
#define PISO_H_


#include "ObjetoSimple.h"

class Piso: public ObjetoSimple {
public:
	Piso();
	virtual ~Piso();
	void render(Luz* luz, Vista* view, glm::mat4 model_matrix);

};

#endif /* PISO_H_ */
