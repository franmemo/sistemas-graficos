/*
 * ShaderRuido.h
 *
 *  Created on: 12/08/2013
 *      Author: nacho
 */

#ifndef SHADERRUIDO_H_
#define SHADERRUIDO_H_

#include "Shader.h"

class ShaderRuido: public Shader {
public:
	ShaderRuido();
	virtual ~ShaderRuido();
};

#endif /* SHADERRUIDO_H_ */
