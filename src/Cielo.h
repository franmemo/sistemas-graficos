/*
 * Cielo.h
 *
 *  Created on: 10/08/2013
 *      Author: nacho
 */

#ifndef CIELO_H_
#define CIELO_H_

#include "Esfera.h"
#include "ObjetoSimple.h"

class Cielo: public ObjetoSimple {
public:
	Cielo();
	virtual ~Cielo();
	void render(Luz* luz, Vista* vista, glm::mat4 model_matrix);
private:
	Esfera esfera;
};

#endif /* CIELO_H_ */
