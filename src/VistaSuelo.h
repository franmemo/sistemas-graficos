#ifndef __VISTA_SUELO_H
#define __VISTA_SUELO_H 

#pragma once 
#include "Vista.h"

class VistaSuelo: public Vista{
public:
	void moveMouse(int x, int y, int max_x, int max_y);
	void key(char cAscii, int max_x, int max_y);

	VistaSuelo(const glm::vec3 eye, const glm::vec3 at, const glm::vec3 up);
	~VistaSuelo();
private:
};

#endif /* __VISTA_SUELO_H  */
