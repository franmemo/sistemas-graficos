#ifndef SHADER_H
#define SHADER_H

#include <GL/glew.h>
#include <GL/freeglut.h>
#include <string>
#include <SOIL/SOIL.h>
#include <glm/glm.hpp>
#include "Definiciones.h"
#include <vector>
#include "MatrixSetter.h"
#include "PropertySetter.h"

#include "Luz.h"
#include "Vista.h"
#include "MatrixSetter.h"
#include "FrameBuffer.h"

class Shader: public PropertySetter {
public:
	static void initFBO();
    bool	compileShaderVertex( const char * shaderSrcPath );
    bool	compileShaderFragment( const char * shaderSrcPath );
    bool	link();
	bool	validate();
    void	use();

    int		getHandle();
    bool	isLinked();

    void	bindAttribLocation( GLuint location, const char * name);
    void	bindFragDataLocation( GLuint location, const char * name );

    ~Shader();
    void	setUniform( const char *name, float x, float y, float z);
    void	setUniform( const char *name, const glm::vec2 & v);
    void	setUniform( const char *name, const glm::vec3 & v);
    void	setUniform( const char *name, const glm::vec4 & v);
    void	setUniform( const char *name, const glm::mat4 & m);
    void	setUniform( const char *name, const glm::mat3 & m);
    void	setUniform( const char *name, float val );
    void	setUniform( const char *name, int val );
    void	setUniform( const char *name, bool val );
    void 	setMatrixes(Luz* luz, Vista* vista, glm::mat4 model_matrix);
    virtual void setupFBO();
    virtual void teardownFBO();
	static FrameBuffer* fbo;
protected:
    bool linked;
    bool FBOsetup;
	glm::vec4 light_position;
    GLuint handle;
	GLuint vertShader;
	GLuint fragShader;
	MatrixSetter* ms;

    int  getUniformLocation(const char * name );


    Shader();
};



#endif // SHADER_H
