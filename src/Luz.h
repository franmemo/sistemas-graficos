/*
 * Luz.h
 *
 *  Created on: 10/08/2013
 *      Author: nacho
 */

#ifndef LUZ_H_
#define LUZ_H_

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "Definiciones.h"
#include "PropertySetter.h"

#include "Vista.h"

class Luz {
public:
	Luz();
	Luz(glm::vec4 &posicion, glm::vec3& intensity);
	virtual ~Luz();
	void setLightProperties(PropertySetter* shader, Vista* vista);
	void setPosition(glm::vec4& position);
	glm::vec4& getPosition();
	glm::mat4 getViewMatrix();
	glm::mat4 getProjectionMatrix();
private:
	glm::vec4 position;
	glm::vec3 intensity;
	glm::mat4 view_matrix;
	glm::mat4 projection_matrix;
};

#endif /* LUZ_H_ */
