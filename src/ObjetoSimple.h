/*
 * ObjetoSimple.h
 *
 *  Created on: 14/08/2013
 *      Author: nacho
 */

#ifndef OBJETOSIMPLE_H_
#define OBJETOSIMPLE_H_

#include "Objeto.h"
#include "Material.h"

class ObjetoSimple: public Objeto {
public:
	ObjetoSimple();
	virtual ~ObjetoSimple();
	virtual void forceShader(Shader* shader);
	virtual void restoreDefaultShader();
protected:
	Shader* shader, *defshader;
	Material* material;
};

#endif /* OBJETOSIMPLE_H_ */
