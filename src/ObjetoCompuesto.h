/*
 * ObjetoCompuesto.h
 *
 *  Created on: 14/08/2013
 *      Author: nacho
 */

#ifndef OBJETOCOMPUESTO_H_
#define OBJETOCOMPUESTO_H_

#include "Objeto.h"
#include <map>
#include <string>

class ObjetoCompuesto: public Objeto {
public:
	ObjetoCompuesto();
	virtual ~ObjetoCompuesto();
	void forceShader(Shader* shader);
	void restoreDefaultShader();
protected:
	std::map<std::string, Objeto*> parts;
};

#endif /* OBJETOCOMPUESTO_H_ */
