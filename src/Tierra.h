/*
 * Tierra.h
 *
 *  Created on: 10/08/2013
 *      Author: nacho
 */

#ifndef TIERRA_H_
#define TIERRA_H_
#include "PlanoSubdividido.h"
#include "ObjetoSimple.h"

class Tierra: public ObjetoSimple {
public:
	void render(Luz* luz, Vista* view, glm::mat4 model_matrix);
	Tierra();
	virtual ~Tierra();
private:
	PlanoSubdividido suelo;
};

#endif /* TIERRA_H_ */
