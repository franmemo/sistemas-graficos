/*
 * Escena.h
 *
 *  Created on: 05/07/2013
 *      Author: nacho
 */

#ifndef ESCENA_H_
#define ESCENA_H_


#include <vector>
#include "ObjetoCompuesto.h"

class Escena: public ObjetoCompuesto {
public:
	void render(Luz *luz, Vista* view, glm::mat4 model_matrix);
	Escena(int nivelDiscretizacion, int cantArboles, int cantMonticulos, int cantPicos);
	virtual ~Escena();

private:
/*	int cantArboles;
	std::vector<glm::vec3*> pos_arboles;
	std::vector<glm::vec3*> pos_monticulos;
	*/

	void renderArboles(Luz *luz, Vista* view, glm::mat4 model_matrix);
	void renderMonticulos(Luz *luz, Vista* view, glm::mat4 model_matrix);

};

#endif /* ESCENA_H_ */
