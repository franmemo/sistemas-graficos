/*
 * CacheTexturas.h
 *
 *  Created on: 06/08/2013
 *      Author: nacho
 */

#ifndef CACHETEXTURAS_H_
#define CACHETEXTURAS_H_

#include <string>
#include <map>
#include <queue>
#include "Imagen.h"

class CacheTexturas {
public:
	static CacheTexturas& getInstance();
	Imagen* cargarTextura(std::string texture_id);
	static void cerrar();
private:
	CacheTexturas();
	std::map<std::string, std::string> rutas;
	std::map<std::string, Imagen*> imagenes;
	static CacheTexturas* instance;
	~CacheTexturas();

};

#endif /* CACHETEXTURAS_H_ */
