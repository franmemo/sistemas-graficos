#include "Material.h"
#include <iostream>

void Material::setMaterialProperties(Shader* shader) {
	std::list<Propiedad*>::iterator it;

	for (it = propiedades.begin(); it != propiedades.end(); ++it) {
		(*it)->applyPropiedad(shader, this);
	}
}


Material::Material(float red, float green, float blue, float shininess):
    Kd(red, green, blue),
    Ks(red, green, blue),
    Ka(red, green, blue),
    shininess(shininess){
}

Material::~Material() {
}

void Material::addPropiedad(Propiedad* propiedad) {
	propiedades.push_back(propiedad);
}

void Material::setKa(glm::vec3 ka) {
	Ka = ka;
}

void Material::setKd(glm::vec3 kd) {
	Kd = kd;
}

void Material::setKs(glm::vec3 ks) {
	Ks = ks;
}

void Material::setShininess(float shininess) {
	this->shininess = shininess;
}


glm::vec3 Material::getKa() const {
	return Ka;
}

glm::vec3 Material::getKd() const {
	return Kd;
}

glm::vec3 Material::getKs() const {
	return Ks;
}

float Material::getShininess() const {
	return shininess;
}
