/*
 * Opacidad.cpp
 *
 *  Created on: 06/08/2013
 *      Author: nacho
 */

#include "Opacidad.h"
#include "Material.h"

#define MAX_GRADO 10.0f
#define ACOTAR(x) (((x) >= 0.0f)? ((x) <= MAX_GRADO)? (x) : MAX_GRADO : 0.0f)

Opacidad::Opacidad(float grado) :
		grado(ACOTAR(grado)) {

}

Opacidad::~Opacidad() {
}

void Opacidad::applyPropiedad(Shader* shader, Material* material) {
	shader->setUniform("Material.Ks", material->getKs() * grado);
	shader->setUniform("Material.Kd", material->getKd());
	shader->setUniform("Material.Ka", material->getKa());
	shader->setUniform("Material.Shininess", material->getShininess());
}
