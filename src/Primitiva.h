/*
 * Primitiva.h
 *
 *  Created on: 10/08/2013
 *      Author: nacho
 */

#ifndef PRIMITIVA_H_
#define PRIMITIVA_H_

#include <GL/glew.h>
#include "glutWindow.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "Definiciones.h"
#include "Vista.h"
#include "Shader.h"


class Primitiva {
public:
	Primitiva();
	virtual ~Primitiva();
	virtual void render() = 0;

protected:
	GLfloat* vertex_buffer;
	GLuint* index_buffer;
	GLfloat* texture_buffer;
	GLfloat* tangent_buffer;
	GLfloat* normal_buffer;

	GLuint vaoHandle;
	unsigned int vertex_buffer_size;
	unsigned int index_buffer_size;
	unsigned int normal_buffer_size;
	unsigned int tangent_buffer_size;
	unsigned int texture_buffer_size;
};

#endif /* PRIMITIVA_H_ */
