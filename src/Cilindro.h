#ifndef __CILINDRO_H
#define __CILINDRO_H 

#include <vector>
#include "Primitiva.h"

class Cilindro: public Primitiva {
public:
	virtual void render();
	Cilindro(const float radio, const float altura, const unsigned int segmentos);
	virtual ~Cilindro();
private:
	void crearCara(const float phi, unsigned& ivec, unsigned& itex, unsigned& inorm, unsigned& itang);
	void crearTapa(int sentido, unsigned& ivec, unsigned& itex, unsigned& inorm, unsigned& itang);
	float radio;
	float altura;
	unsigned int segmentos;
};
#endif
