/*
 * Utilitis.h
 *
 *  Created on: 21/09/2013
 *      Author: francisco
 */

#ifndef UTILITIS_H_
#define UTILITIS_H_
#include <glm/gtc/matrix_transform.hpp>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <glm/glm.hpp>
#include <glm/gtx/transform2.hpp>
#include <glm/gtx/projection.hpp>
#include <vector>
#include <cmath>

class Utilities {
private:

    glm::mat4 view_matrix;

    Utilities();

	void createBody();

	static float recursiveInterpolationBezier(float* vPointsX, int length,float t);

	static float interpolationBSpline(float* vPointsX,float t);
	int valor;
public:
	void getTangentNormalBuffer(int tamanioCalidadY, int tamanioCalidadX, std::vector<float>& bodyTang_bufferT1, std::vector<float>& bodyNormal_bufferT1,GLfloat* leaf_vertex_buffer,bool horario);
	float modulo(glm::vec3 vector);
	float modulo(float x,float y, float z);
	glm::vec3 producto(glm::vec3 v1, glm::vec3 v2);
	static float recursiveInterpolationBezierEspecial(float* vPointsX, int length,float t);
	float interpolationBSplineEspecial(float vPointsX[][3] , int length,float t);

	float interpolationBSplineEspecialNorm(float vPointsX[][3] , int length,float t);
	float interpolationBSplineEspecialTang(float vPointsX[][3] , int length,float t);
	float interpolationBSplineTang(float* vPointsX,float t);

	static Utilities* getEntity(GLuint sProgramHandle);
	static Utilities* getEntity();
	static float getValueByAproximation(float x1, float y1, float x2, float y2, float x);
	virtual ~Utilities();
	void changeObjectColor(float r, float g, float b);


};

#endif /* UTILITIS_H_ */
