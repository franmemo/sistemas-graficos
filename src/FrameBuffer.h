/*
 * FrameBuffer.h
 *
 *  Created on: 15/08/2013
 *      Author: nacho
 */

#ifndef FRAMEBUFFER_H_
#define FRAMEBUFFER_H_

#include <GL/glew.h>
#include <GL/freeglut.h>
#include "Definiciones.h"

class FrameBuffer {
public:
	FrameBuffer();
	virtual ~FrameBuffer();
	void bind();
	void unbind();
private:
	GLuint shadowFBO;
	GLuint depthTex;
	bool binded;
};

#endif /* FRAMEBUFFER_H_ */
