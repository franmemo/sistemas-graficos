#include "Parante.h"
#include "Definiciones.h"
#include "FactoryMateriales.h"
#include "CacheShaders.h"

#include <iostream>


Parante::Parante(unsigned int pisos):
    parante(0.07f, (2) * (pisos), 1) {
    material = FactoryMateriales::createParante();
    defshader = shader = CacheShaders::getInstance().getShader(std::string("parante"));
}

Parante::~Parante() {
    delete material;
}


void Parante::render(Luz *luz, Vista* view, glm::mat4 model_matrix) {
	shader->use();
	if (defshader == shader) {
		luz->setLightProperties(shader, view);
		material->setMaterialProperties(shader);
	}
	shader->setMatrixes(luz, view, model_matrix);
	parante.render();
}
