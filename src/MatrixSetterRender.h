/*
 * MatrixSetterRender.h
 *
 *  Created on: 14/08/2013
 *      Author: nacho
 */

#ifndef MATRIXSETTERRENDER_H_
#define MATRIXSETTERRENDER_H_

#include "MatrixSetter.h"

class MatrixSetterRender: public MatrixSetter {
public:
	MatrixSetterRender();
	virtual ~MatrixSetterRender();
	virtual void setMatrixes(PropertySetter* ps, Luz* luz, Vista* vista, glm::mat4 model_matrix);
};

#endif /* MATRIXSETTERRENDER_H_ */
