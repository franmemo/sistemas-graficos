/*
 * ShaderRuido.cpp
 *
 *  Created on: 12/08/2013
 *      Author: nacho
 */

#include "ShaderRuido.h"
#include "MatrixSetterRender.h"

ShaderRuido::ShaderRuido() {
	this->compileShaderVertex("shaders/ruido.vs");
	this->compileShaderFragment("shaders/texture.frag");
	glBindAttribLocation(handle, 0, "VertexPosition");
	glBindAttribLocation(handle, 1, "VertexNormal");
	glBindAttribLocation(handle, 2, "VertexTexCoord");
	ms = new MatrixSetterRender();
}

ShaderRuido::~ShaderRuido() {
	delete ms;
}

