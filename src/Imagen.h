/*
 * Imagen.h
 *
 *  Created on: 06/08/2013
 *      Author: nacho
 */

#ifndef IMAGEN_H_
#define IMAGEN_H_

#include <string>
#include <SOIL/SOIL.h>

class Imagen {
public:
	Imagen(std::string file);
	Imagen(char* file);
	virtual ~Imagen();

	int getAncho() const;

	int getLargo() const;

	int getId() const;

private:
	int ancho;
	int largo;
	unsigned int id;

	void loadFile(const char* file);
};

#endif /* IMAGEN_H_ */
