/*
 * FactoryPropiedades.cpp
 *
 *  Created on: 06/08/2013
 *      Author: nacho
 */

#include "FactoryMateriales.h"
#include "Opacidad.h"
#include "Textura.h"


Material* FactoryMateriales::createCopa() {
	Material* mat = new Material(0.1, 0.4, 0.1, 100.0);
	mat->addPropiedad(new Opacidad(0.2));
	mat->addPropiedad(new Textura("copa", TEXTURA));
//	mat->addPropiedad(new Textura("copa-normal", NORMALMAP, 0.6));
	return mat;
}

Material* FactoryMateriales::createTronco() {
	Material* mat = new Material(0.4, 0.2, 0.2, 100.0);
	mat->addPropiedad(new Opacidad(0.3));
	mat->addPropiedad(new Textura("tronco", TEXTURA));
	mat->addPropiedad(new Textura("tronco-normal", NORMALMAP, 0.6));
	return mat;
}

Material* FactoryMateriales::createGrua() {
	Material* mat = new Material(1., 1., 0.5, 100.f);
	mat->addPropiedad(new Opacidad(0.7f));
	mat->addPropiedad(new Textura("grua", TEXTURA));
	mat->addPropiedad(new Textura("grua-normal", NORMALMAP, 0.4));
	return mat;
}

Material* FactoryMateriales::createColumna() {
	Material* mat = new Material(0.5f, 0.5f, 0.5f, 1.0f);
	mat->addPropiedad(new Opacidad(.3));
	mat->addPropiedad(new Textura("columna", TEXTURA));
//	mat->addPropiedad(new Textura("columna-normal", NORMALMAP, 0.3));
	return mat;
}

Material* FactoryMateriales::createFondo() {
	Material* mat = new Material(0.f, .6f, .8f, 100.f);
	mat->addPropiedad(new Opacidad(1.0f));
	mat->addPropiedad(new Textura("fondo", TEXTURA, 1.0));
	return mat;
}

Material* FactoryMateriales::createVidrioAfuera() {
	Material* mat = new Material(0.73f, 0.80f, 0.89f, 1.0f);
	mat->addPropiedad(new Opacidad(0.2));
	mat->addPropiedad(new Textura("fondo", TEXTURA, 0.4));
	return mat;
}

Material* FactoryMateriales::createVidrioAdentro() {
	Material* mat = new Material(0.7f, 0.7f, 0.7f, 1.0f);
	mat->addPropiedad(new Opacidad(1.0));
	mat->addPropiedad(new Textura("vidrio", TEXTURA, 0.4));

	return mat;
}

Material* FactoryMateriales::createPiso() {
	Material* mat = new Material(0.5f, 0.5f, 0.5f, 10.0f);
	mat->addPropiedad(new Opacidad(0.0));
	mat->addPropiedad(new Textura("piso", TEXTURA));
	mat->addPropiedad(new Textura("piso-normal", NORMALMAP, 0.3));
	return mat;
}

Material* FactoryMateriales::createTierra() {
	Material* mat = new Material(0., 0.4, 0.27, 0.005f);
	mat->addPropiedad(new Opacidad(.1));
	mat->addPropiedad(new Textura("tierra", TEXTURA, .6));
//	mat->addPropiedad(new Textura("tierra-normal", NORMALMAP, 0.7));
	return mat;
}

Material* FactoryMateriales::createParded() {
	Material* mat = new Material(1.0f, 0.8f, 0.8f, 0.1f);
	mat->setKd(glm::vec3(0.f));
	mat->addPropiedad(new Opacidad(1.0));
	mat->addPropiedad(new Textura("pared", TEXTURA, 1));
	mat->addPropiedad(new Textura("pared-normal", NORMALMAP, .7f));
	return mat;
}
Material* FactoryMateriales::createMonticulo() {
	Material* mat = new Material(0.63, 0.56, 0.38, 1.f);
	mat->addPropiedad(new Opacidad(0.4));
	mat->addPropiedad(new Textura("monticulo", TEXTURA));
	mat->addPropiedad(new Textura("monticulo-normal", NORMALMAP));
	return mat;
}

Material* FactoryMateriales::createParante() {
	Material* mat = new Material(0.5f, 0.5f, 0.5f, 1.0f);
	mat->addPropiedad(new Opacidad(1.0));
	mat->addPropiedad(new Textura("fondo", TEXTURA,0.4));
//	mat->addPropiedad(new Transparencia(0.2));
	return mat;
}


Material* FactoryMateriales::createMonticulo2() {
	Material* mat = new Material(0.63, 0.56, 0.38, 1.f);
	mat->addPropiedad(new Opacidad(0.4));
	mat->addPropiedad(new Textura("monticulo2", TEXTURA));
	return mat;
}
Material* FactoryMateriales::createDucto() {
	Material* mat = new Material(0.9, 0.91, 0.98, 100.f);
	mat->addPropiedad(new Opacidad(1.5));
	mat->addPropiedad(new Textura("ducto", TEXTURA));
	mat->addPropiedad(new Textura("ducto-normal", NORMALMAP, .8));
	return mat;
}

Material* FactoryMateriales::createEscalera() {
	Material* mat = new Material(0.5f, 0.5f, 0.5f, 100.0f);
	mat->addPropiedad(new Opacidad(0.0));
	mat->addPropiedad(new Textura("escalera", TEXTURA));
	mat->addPropiedad(new Textura("escalera-normal", NORMALMAP, 0.7));
	return mat;
}

FactoryMateriales::FactoryMateriales() {

}


FactoryMateriales::~FactoryMateriales() {
}

