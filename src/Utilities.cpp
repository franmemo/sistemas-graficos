/*
 * Utilitis.cpp
 *
 *  Created on: 21/09/2013
 *      Author: francisco
 */
/* */
#include "Utilities.h"
static Utilities* entity;
#include <iostream>
//el parcial es el 1 de nov creo.

Utilities::Utilities() {
}

glm::vec3 Utilities::producto(glm::vec3 v1, glm::vec3 v2){
	return glm::cross(v1, v2);
}

Utilities* Utilities::getEntity(){
	return entity;
}

Utilities::~Utilities() {
	// TODO Auto-generated destructor stub
}

float Utilities::getValueByAproximation(float x1, float y1, float x2, float y2, float x){
	return (((x - x1)/(x2 - x1))*(y2 -y1)) +  y1;
}
float Utilities::interpolationBSpline(float* vPointsX,float t){
	float b0 = 0.5*(1-t)*(1-t);
	float b1 = 0.5+t-(t*t);
	float b2 = 0.5*(t)*(t);
	return (b0*vPointsX[0])+(b1*vPointsX[1])+(b2*vPointsX[2]);
}

float Utilities::interpolationBSplineTang(float* vPointsX,float t){
	float b0 = t-1;
	float b1 = 1-(2*t);
	float b2 = t;
	return (b0*vPointsX[0])+(b1*vPointsX[1])+(b2*vPointsX[2]);
}
float Utilities::modulo(glm::vec3 vector){
	return sqrt((vector.x*vector.x) + (vector.y*vector.y) +(vector.z*vector.z));
}
float Utilities::modulo(float x,float y, float z){
	return sqrt((x*x) + (y*y) +(z*z));
}
float Utilities::interpolationBSplineEspecialNorm(float vPointsX[][3] , int length,float t){
	int indexX  = t*length;
	float newT = t*length;
	newT = newT - indexX;
	if(indexX == length){
		indexX = length-1;
		newT = 1;
	}
	float b0 = 1;
	float b1 = -2;
	float b2 = 1;
	return (b0*vPointsX[indexX][0])+(b1*vPointsX[indexX][1])+(b2*vPointsX[indexX][2]);

}

void Utilities::getTangentNormalBuffer(int tamanioCalidadY, int tamanioCalidadX, std::vector<float>& bodyTang_bufferT1, std::vector<float>& bodyNormal_bufferT1,GLfloat* leaf_vertex_buffer,bool horario) {
	glm::vec3 vacio = glm::vec3 (0,0,0);
	glm::vec3 costado = glm::vec3 (0,0,0);
	glm::vec3 sentido = glm::vec3 (0,0,0);

	for (int paso = 0; paso < tamanioCalidadY; paso++) {
		//cout <<"nomrales"<<endl;
		for (int valX = 0; valX < tamanioCalidadX; valX ++) {
			int x = (valX * 3) + (paso*tamanioCalidadY*3);
			glm::vec3 ptoActual = glm::vec3(leaf_vertex_buffer[x],
					leaf_vertex_buffer[x + 1], leaf_vertex_buffer[x + 2]);
			glm::vec3 normal = glm::vec3(leaf_vertex_buffer[x],
					leaf_vertex_buffer[x + 1], leaf_vertex_buffer[x + 2]);
			//si es el ultimo de los ultimos
			int ventana = (tamanioCalidadX )* 3;
			if (valX == (tamanioCalidadX - 1) && paso == (tamanioCalidadY - 1)) {
				glm::vec3 ptoProx = glm::vec3(leaf_vertex_buffer[x - 3],
						leaf_vertex_buffer[x + 1 - 3],
						leaf_vertex_buffer[x + 2 - 3]);
				glm::vec3 ptoLateral = glm::vec3(
						leaf_vertex_buffer[x - ventana],
						leaf_vertex_buffer[x + 1 - ventana],
						leaf_vertex_buffer[x + 2 - ventana]);
				sentido = ptoActual - ptoProx;
				costado = ptoActual - ptoLateral;
			} else if (valX == (tamanioCalidadX - 1)) {
				glm::vec3 ptoProx = glm::vec3(leaf_vertex_buffer[x - 3],leaf_vertex_buffer[x + 1 - 3],leaf_vertex_buffer[x + 2 - 3]);
				glm::vec3 ptoLateral = glm::vec3(leaf_vertex_buffer[x + ventana],leaf_vertex_buffer[x + 1 + ventana],leaf_vertex_buffer[x + 2 + ventana]);
				sentido =  ptoActual - ptoProx;
				costado = ptoLateral - ptoActual ;
			} else if (paso == (tamanioCalidadY - 1)) {
				glm::vec3 ptoProx = glm::vec3(leaf_vertex_buffer[x + 3],
						leaf_vertex_buffer[x + 1 + 3],
						leaf_vertex_buffer[x + 2 + 3]);
				glm::vec3 ptoLateral = glm::vec3(leaf_vertex_buffer[x - ventana],leaf_vertex_buffer[x + 1 - ventana],leaf_vertex_buffer[x + 2 - ventana]);
				sentido = ptoProx - ptoActual;
				costado = ptoActual - ptoLateral;
				if(costado == vacio ){
					ptoLateral = glm::vec3(leaf_vertex_buffer[x - ventana + 3],leaf_vertex_buffer[x + 1 - ventana + 3],leaf_vertex_buffer[x + 2 - ventana + 3]);
					costado = ptoActual - ptoLateral ;
				}

			} else {
				glm::vec3 ptoProx = glm::vec3(leaf_vertex_buffer[x + 3],leaf_vertex_buffer[x + 1 + 3],leaf_vertex_buffer[x + 2 + 3]);
				glm::vec3 ptoLateral = glm::vec3(leaf_vertex_buffer[x + ventana],leaf_vertex_buffer[x + 1 + ventana],leaf_vertex_buffer[x + 2 + ventana]);
				sentido = ptoProx - ptoActual;
				costado = ptoLateral - ptoActual;
				if(costado == vacio ){
					ptoLateral = glm::vec3(leaf_vertex_buffer[x + ventana+3],leaf_vertex_buffer[x + 1 + ventana+3],leaf_vertex_buffer[x + 2 + ventana+3]);
					costado = ptoLateral - ptoActual;
				}
				if(sentido == vacio ){
					glm::vec3 ptoProx = glm::vec3(leaf_vertex_buffer[x + 6],leaf_vertex_buffer[x + 1 + 6],leaf_vertex_buffer[x + 2 + 6]);
					sentido = ptoProx - ptoActual;
				}
			}
			if(horario){
				normal = glm::cross(costado, sentido);
			}else{
				normal = glm::cross(sentido, costado);
			}
			normal = glm::normalize(normal);
			sentido = glm::normalize(sentido);

			//cout<<normal.x<<";"<<normal.y<<";"<<normal.z<<endl;
			bodyTang_bufferT1.push_back(sentido.x);
			bodyTang_bufferT1.push_back(sentido.y);
			bodyTang_bufferT1.push_back(sentido.x);

			bodyNormal_bufferT1.push_back(normal.x);
			bodyNormal_bufferT1.push_back(normal.y);
			bodyNormal_bufferT1.push_back(normal.z);
		}
	}
}
float Utilities::interpolationBSplineEspecialTang(float vPointsX[][3] , int length,float t){
	int indexX  = t*length;
	float newT = t*length;
	newT = newT - indexX;
	if(indexX == length){
		indexX = length-1;
		newT = 1;
	}
	return interpolationBSplineTang(vPointsX[indexX],newT);
}
float Utilities::interpolationBSplineEspecial(float vPointsX[][3] , int length,float t){
	int indexX  = t*length;
	float newT = t*length;
	newT = newT - indexX;
	if(indexX == length){
		indexX = length-1;
		newT = 1;
	}
	return interpolationBSpline(vPointsX[indexX],newT);
}
float Utilities::recursiveInterpolationBezierEspecial(float* vPointsX, int length,float t){
	int castVaule = (int)(t*length);
	float * vtreePoint = new float[4];

	vtreePoint[0] = vPointsX[(castVaule * 4)];
	vtreePoint[1] = vPointsX[(castVaule * 4)+1];
	vtreePoint[2] = vPointsX[(castVaule * 4)+2];
	vtreePoint[3] = vPointsX[(castVaule * 4)+3];
	t = t-(castVaule * (float)((float)1/(float)length));
	t = t*length;
	//float val2 = Utilities::recursiveInterpolationBezier(vtreePoint,4,t);
	float val1 = (((1-t)*(1-t)*(1-t))*vtreePoint[0])+(3*t*(1-t)*(1-t)*vtreePoint[1])+(t*t*vtreePoint[2]*3*(1-t))+(vtreePoint[3]*(t*t*t));
	return val1;
}

float Utilities::recursiveInterpolationBezier(float* vPointsX, int length,float t){
	if(length < 2){ // esto es por si se pasa un parametro mal o algo asi
		// no deberia pasar nunca que sea menor a 2.
		return 0;
	}
	if(length == 2){
		return (((1-t)*vPointsX[0])+(vPointsX[1])*t);
	}
	float* vPointNewY = new float[length-1];

	for(int i=0;i< length-1; i++){
		vPointNewY[i] = ((1-t)*vPointsX[i])+(vPointsX[i+1]*t);
	}
	return Utilities::recursiveInterpolationBezier(vPointNewY,length-1,t);
}

