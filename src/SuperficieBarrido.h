/*
 * SuperficieBarrido.h
 *
 *  Created on: 04/07/2013
 *      Author: nacho
 */

#ifndef SUPERFICIEBARRIDO_H_
#define SUPERFICIEBARRIDO_H_

#include "Curva.h"
#include "Superficie.h"

class SuperficieBarrido: public Superficie {
private:
	Curva& recorrido;
	float ancho_lateral;
	float altura_lateral;
	glm::vec3 normal;

public:
	void generarMalla();
	SuperficieBarrido(float ancho_lateral, float altura_lateral, Curva& recorrido, glm::vec3& normal);
	virtual ~SuperficieBarrido();
};

#endif /* SUPERFICIEBARRIDO_H_ */
