#ifndef __GRILLA_H
#define __GRILLA_H 

#include "Primitiva.h"

class Grilla: public Primitiva{
public:
	Grilla(int size = 10);
	~Grilla();
	virtual void render();
};

#endif /* __GRILLA_H */
