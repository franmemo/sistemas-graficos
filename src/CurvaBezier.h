#pragma once

#include "Curva.h"
#include <glm/glm.hpp>
#include <vector>

class CurvaBezier :
	public Curva
{
public:
	CurvaBezier(int pasos);
	CurvaBezier(glm::vec3 origen, int pasos);
	CurvaBezier(std::vector<glm::vec3> &puntos, int pasos);

	glm::vec3 siguiente();
	virtual ~CurvaBezier(void);

	float b0(float u);
	float b1(float u);
	float b2(float u);
	float b3(float u);
};

