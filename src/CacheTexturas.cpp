/*
 * CacheTexturas.cpp
 *
 *  Created on: 06/08/2013
 *      Author: nacho
 */

#include "CacheTexturas.h"
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <iostream>
using namespace std;

CacheTexturas* CacheTexturas::instance = NULL;

CacheTexturas::CacheTexturas() {
	//TODO: agregar las rutas que falten, junto con las imagenes
	rutas["fondo"] = "textures/fondo2.jpg";

	rutas["monticulo"] = "textures/arena.jpg";
	rutas["monticulo-normal"] = "textures/arena-normal.jpg";
	rutas["monticulo2"] = "textures/arena2.png";
	rutas["monticulo2-normal"] = "textures/arena2-normal.png";

	rutas["pared"] = "textures/pared.jpg";
	rutas["pared-normal"] = "textures/pared-normal.jpg";

	rutas["copa"] = "textures/copa.jpg";
	rutas["copa-normal"] = "textures/copa-normal.jpg";
	rutas["tronco"] = "textures/tronco2.jpg";
	rutas["tronco-normal"] = "textures/tronco-normal.jpg";

	rutas["tierra"] = "textures/pasto.jpg";
	rutas["tierra-normal"] = "textures/pasto-normal.jpg";

	rutas["columna"] = "textures/grua.jpg";
	rutas["columna-normal"] = "textures/grua-normal.jpg";

	rutas["escalera"] = "textures/grua.jpg";
	rutas["escalera-normal"] = "textures/grua-normal.jpg";

	rutas["ducto"] = "textures/ducto.jpg";
	rutas["ducto-normal"] = "textures/ducto-normal.jpg";

	//rutas["vidrio"] = "textures/fondo2.jpg";

	rutas["piso"] = "textures/losa.png";
	rutas["piso-normal"] = "textures/losa-normal.png";

	rutas["grua"] = "textures/grua.jpg";
	rutas["grua-normal"] = "textures/grua-normal.jpg";

	rutas["parante"] = "textures/parante.jpg";
}

void CacheTexturas::cerrar() {
	if (instance)
		delete instance;
	instance = NULL;
}

CacheTexturas::~CacheTexturas() {
	std::map<std::string,Imagen*>::iterator iter;
	for(iter = imagenes.begin(); iter != imagenes.end(); iter++) {
		delete (*iter).second;
	}
}

CacheTexturas& CacheTexturas::getInstance() {
	if (!instance) {
		instance = new CacheTexturas();
	}
	return *instance;
}

Imagen* CacheTexturas::cargarTextura(std::string texture_id) {
	Imagen* imagen = NULL;
	if (imagenes.find(texture_id) != imagenes.end())
		imagen = imagenes[texture_id];
	else {
		imagen = new Imagen(rutas[texture_id]);
		imagenes[texture_id] = imagen;
	}
	return imagen;
}
