/*
 * Copa.h
 *
 *  Created on: 10/08/2013
 *      Author: nacho
 */

#ifndef COPA_H_
#define COPA_H_

#include "SuperficieRevolucion.h"
#include "ObjetoSimple.h"

class Copa: public ObjetoSimple {
public:
	Copa(int nivelDiscretizacion);
	virtual ~Copa();
	void render(Luz* luz, Vista* view, glm::mat4 model_matrix);

private:
	int nivelDiscretizacion;
	SuperficieRevolucion* copa;
	float ponderacion;
};

#endif /* COPA_H_ */
