#pragma once

#include "Shader.h"

#include <glm/glm.hpp> 

#include <vector>
#include "Primitiva.h"

class Esfera : public Primitiva {
public:
	Esfera();
	void render();
protected:
    std::vector<float> verticesBuffer;
	std::vector<float> texCordsBuffer;
	std::vector<float> tangentesBuffer;

	void crearCara(float t, float o);
    void createSpiralSphere();
};
