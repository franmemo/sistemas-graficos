/*
 * CacheShaders.cpp
 *
 *  Created on: 10/08/2013
 *      Author: nacho
 */

#include "CacheShaders.h"
#include "ShaderNormales.h"
#include "ShaderTexturaSimple.h"
#include "ShaderReflexion.h"

#include "ShaderRuido.h"


CacheShaders* CacheShaders::instance = NULL;

CacheShaders::CacheShaders() {
}

bool CacheShaders::isActive(Shader* shader) {
	return active == shader;
}

void CacheShaders::activate(Shader* shader) {
	if(active)
		active->teardownFBO();
	active = shader;
}

CacheShaders::~CacheShaders() {
	delete shref;
	delete shtex;
	delete shnorm;

}

CacheShaders& CacheShaders::getInstance() {
	if (!instance)
		initShaders();
	return *instance;
}

Shader* CacheShaders::getShader(std::string object_id) {
	return shadersMap[object_id];
}

void CacheShaders::initShaders() {
	instance = new CacheShaders();
	Shader* shref = new ShaderReflexion();
	shref->link(); shref->validate();
	Shader* shtex = new ShaderTexturaSimple();
	shtex->link(); shtex->validate();
	Shader* shnorm = new ShaderNormales();
	shnorm->link(); shnorm->validate();


	Shader* ruido = new ShaderRuido();
	ruido->link(); ruido->validate();


	instance->active = 0;
	instance->shref = shref;
	instance->shtex = shtex;
	instance->shnorm = shnorm;

	instance->shadersMap["monticulo"] = shtex;
	instance->shadersMap["cielo"] = shtex;
	instance->shadersMap["tierra"] = shtex;
	instance->shadersMap["columna"] = shtex;
	instance->shadersMap["piso"] = shnorm;
	instance->shadersMap["escalera"] = shnorm;
	instance->shadersMap["pared"] = shnorm;
	instance->shadersMap["parante"] = shref;
	instance->shadersMap["copa"] = ruido;
	instance->shadersMap["tronco"] = shnorm;
	instance->shadersMap["grua"] = shnorm;

	instance->shadersMap["transparencia"] = shref;
	instance->shadersMap["ducto"] = shnorm;
}

void CacheShaders::close() {
	if (instance)
		delete instance;
	instance = NULL;
}
