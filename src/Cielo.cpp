/*
 * Cielo.cpp
 *
 *  Created on: 10/08/2013
 *      Author: nacho
 */

#include "Cielo.h"
#include "CacheShaders.h"
#include "FactoryMateriales.h"
#include <iostream>

Cielo::Cielo() {
	material = FactoryMateriales::createFondo();
	defshader = shader = CacheShaders::getInstance().getShader("cielo");
}

Cielo::~Cielo() {
	delete material;
}

void Cielo::render(Luz* luz, Vista* view, glm::mat4 model_matrix) {
	shader->use();
	if (defshader == shader) {
		luz->setLightProperties(shader, view);
		material->setMaterialProperties(shader);
	}
	shader->setMatrixes(luz, view, glm::scale(model_matrix, glm::vec3(500.f,500.f,500.f)));
	esfera.render();
}
