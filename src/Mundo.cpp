/*
 * Mundo.cpp
 *
 *  Created on: 06/07/2013
 *      Author: nacho
 */

#include "Mundo.h"
#include <glm/glm.hpp>


#include "Escena.h"
#include "Tierra.h"
#include "Cielo.h"

Mundo::Mundo() {
	//parts["edificio"] = new Edificio(10);
	//parts["grua"] = new Grua(13, 12);
	//parts["escena"] = new Escena(20, 5, 2, 7);
	parts["tierra"] = new Tierra();
	//parts["cielo"] = new Cielo();
}


void Mundo::render(Luz* luz, Vista* view, glm::mat4 model_matrix) {
	//parts["escena"]->render(luz, view, model_matrix);
	parts["tierra"]->render(luz, view, glm::scale(model_matrix, glm::vec3(5)));
	//parts["cielo"]->render(luz, view, model_matrix);
	glm::mat4 model_matrix_edificio = glm::scale(model_matrix, glm::vec3(1.,1.f,0.75f));
	model_matrix_edificio = glm::rotate(model_matrix_edificio, 180.f, glm::vec3(0.,0.f,1.f));
	//parts["edificio"]->render(luz, view, model_matrix_edificio);
//	glm::mat4 model_matrix_grua = glm::translate(model_matrix, glm::vec3(0.,9.f,0.f));
	//parts["grua"]->render(luz, view, model_matrix_grua);
}


Mundo::~Mundo() {
}

