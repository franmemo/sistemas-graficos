/*
 * Imagen.cpp
 *
 *  Created on: 06/08/2013
 *      Author: nacho
 */

#include "Imagen.h"

#include <iostream>

Imagen::Imagen(std::string file) {
	loadFile(file.c_str());
}

Imagen::Imagen(char* file) {
	loadFile(file);
}

void Imagen::loadFile(const char* file) {
	int image_channels;
	id = SOIL_load_OGL_texture(file, SOIL_LOAD_RGBA, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS |  SOIL_FLAG_NTSC_SAFE_RGB );
}

Imagen::~Imagen() {
}

int Imagen::getAncho() const {
	return ancho;
}

int Imagen::getLargo() const {
	return largo;
}

int Imagen::getId() const {
	return id;
}
