#ifndef __ESCALERA_H
#define __ESCALERA_H
#pragma once

#include "Cubo.h"
#include "ObjetoSimple.h"

class Escalera: public ObjetoSimple {
private:
	Cubo cubo;
	unsigned char escalones;
public:
	Escalera(unsigned char escalones = 12);
	virtual ~Escalera();
	void render(Luz* luz, Vista* view, glm::mat4 model_matrix);
};

#endif /* __ESCALERA_H */
