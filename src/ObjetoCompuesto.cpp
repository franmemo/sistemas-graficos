/*
 * ObjetoCompuesto.cpp
 *
 *  Created on: 14/08/2013
 *      Author: nacho
 */

#include "ObjetoCompuesto.h"

ObjetoCompuesto::ObjetoCompuesto() {
	// TODO Auto-generated constructor stub

}

ObjetoCompuesto::~ObjetoCompuesto() {
	// TODO Auto-generated destructor stub
}

void ObjetoCompuesto::forceShader(Shader* sh) {
	std::map<std::string, Objeto*>::iterator iter;
	for (iter = parts.begin(); iter != parts.end(); iter++) {
		iter->second->forceShader(sh);
	}
}

void ObjetoCompuesto::restoreDefaultShader() {
	std::map<std::string, Objeto*>::iterator iter;
	for (iter = parts.begin(); iter != parts.end(); iter++) {
		iter->second->restoreDefaultShader();
	}
}
