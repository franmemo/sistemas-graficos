#include "Columna.h"
#include "Definiciones.h"
#include "FactoryMateriales.h"
#include "CacheShaders.h"

#include <iostream>


Columna::Columna(unsigned int pisos):
    cilindro(1, (2) * (2), 2) {
    material = FactoryMateriales::createColumna();
    defshader = shader = CacheShaders::getInstance().getShader(std::string("columna"));
}

Columna::~Columna() {
    delete material;
}


void Columna::render(Luz *luz, Vista* view, glm::mat4 model_matrix) {
	shader->use();
	if (defshader == shader) {
		luz->setLightProperties(shader, view);
		material->setMaterialProperties(shader);
	}
	shader->setMatrixes(luz, view, model_matrix);
	cilindro.render();
}
