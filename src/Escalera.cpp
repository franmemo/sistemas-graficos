#include "Escalera.h"

#include "FactoryMateriales.h"
#include "CacheShaders.h"

#include <iostream>

Escalera::Escalera(unsigned char escalones) :
		escalones(escalones) {
	defshader = shader = CacheShaders::getInstance().getShader(std::string("escalera"));
	material = FactoryMateriales::createEscalera();
}

Escalera::~Escalera() {
	delete material;
}

void Escalera::render(Luz *luz, Vista* view, glm::mat4 model_matrix) {
	shader->use();
	if (defshader == shader) {
		material->setMaterialProperties(shader);
		luz->setLightProperties(shader, view);
	}
	model_matrix = glm::translate(model_matrix, glm::vec3(-1.7, 0, -1.1));

	glm::mat4 mEscalateLarge = glm::scale(model_matrix,
			glm::vec3(0.7, 2, 2 / escalones));
	shader->setMatrixes(luz, view, mEscalateLarge);
	cubo.render();
	glm::mat4 mEscalate = glm::scale(glm::mat4(),
			glm::vec3(2 / (escalones - 2), 1,
					2 / escalones));
	glm::mat4 mTraslation = glm::translate(model_matrix,
			glm::vec3(0.7 / 2 + 2 / (escalones - 2) / 2, 0,
					2 / escalones));
	for (int i = 2; i < escalones; i++) {
		shader->setMatrixes( luz, view, mTraslation * mEscalate);
		cubo.render();
		mTraslation = glm::translate(mTraslation,
				glm::vec3(2 / (escalones - 2), 0,
						2 / escalones));
	}
	mTraslation = glm::translate(mTraslation, glm::vec3(0.7 / 4, 0, 0));
	mEscalateLarge = glm::scale(mTraslation,
			glm::vec3(0.7, 2, 1 / escalones));
	shader->setMatrixes(luz, view, mEscalateLarge);
	cubo.render();
}

