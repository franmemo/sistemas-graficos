/*
 * Piso.cpp
 *
 *  Created on: 10/08/2013
 *      Author: nacho
 */

#include "Piso.h"
#include "CacheShaders.h"
#include "FactoryMateriales.h"

Piso::Piso() {
	defshader = shader = CacheShaders::getInstance().getShader(std::string("piso"));
	material = FactoryMateriales::createPiso();
}

Piso::~Piso() {
	delete material;
}

void Piso::render(Luz* luz, Vista* view, glm::mat4 model_matrix) {
	shader->use();
	if (defshader == shader) {
		material->setMaterialProperties(shader);
		luz->setLightProperties(shader, view);
	}
	shader->setMatrixes( luz, view, model_matrix);
	//losa.render();
}
