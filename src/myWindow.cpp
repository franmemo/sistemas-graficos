#include <GL/glew.h>
#include <GL/freeglut.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform2.hpp>
#include <glm/gtx/projection.hpp>

#include "myWindow.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <unistd.h>
#include "CacheTexturas.h"
#include "CacheShaders.h"
#include "MatrixSetterRender.h"


myWindow::myWindow():
	tiempo(0.0f), init(false)
{

}

myWindow::~myWindow(){
	views.clear();
	CacheTexturas::cerrar();
}

void myWindow::OnRender(void) {
	if (updated) {
		Shader::fbo->bind();
		glClear(GL_DEPTH_BUFFER_BIT);
		glClearColor(1.,1.,1.,1.);
		glViewport(0,0,3*800,3*600);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);


		mundo->render(&luz, view, glm::mat4(1.0f));
		glFlush();
		glFinish();
		Shader::fbo->unbind();
		mundo->restoreDefaultShader();
		glViewport(0,0,800,600);
	    glDisable(GL_CULL_FACE);
	    updated = false;
	}
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	mundo->render(&luz, view, glm::mat4(1.0f));
	glutSwapBuffers();

}

void myWindow::OnIdle() {
	this->tiempo += 0.1f;
	glutPostRedisplay();
}

// When OnInit is called, a render context (in this case GLUT-Window)
// is already available!
void myWindow::OnInit() {
// Do your GLEW experiments here:
	if (GLEW_ARB_shading_language_100) {
		std::cout << "GLEW_ARB_shading_language_100" << std::endl;
		int major, minor, revision;
		const GLubyte* sVersion = glGetString(GL_SHADING_LANGUAGE_VERSION_ARB);
		if (glGetError() == GL_INVALID_ENUM) {
			major = 1;
			minor = 0;
			revision = 51;
		} else {
			std::string version((char*) sVersion);
			std::cout << version.c_str() << std::endl;
		}

		CacheShaders::initShaders();
		glEnable(GL_DEPTH_TEST);
		Shader::initFBO();
		mundo = new Mundo();

		this->views.push_back(new VistaGeneral(16., 0.5, 0.5, glm::vec3(0.,0.,0.)/*glm::vec3(0.,0.,10.)*/));
		this->views.push_back(new VistaSuelo(glm::vec3(20.,0.,1.5), glm::vec3(0.,0.,0.), glm::vec3(0.,0.,1.)));


		this->view = this->views[0];
		updated = true;
		init = true;
	}
}

void myWindow::OnResize(int w, int h) {
	glViewport(0, 0, (GLsizei) w, (GLsizei) h);
	this->width = w;
	this->height = h;
}

void myWindow::OnClose(void) {


}

void myWindow::OnMouseDown(int button, int x, int y) {
}

void myWindow::OnMouseUp(int button, int x, int y) {
}

void myWindow::OnMouseWheel(int nWheelNumber, int nDirection, int x, int y) {
}

void myWindow::OnMouseMove(int x, int y){
	this->view->moveMouse(x, y, this->width, this->height);
}

void myWindow::OnLeftMouseDrag(int x, int y){

};

void myWindow::OnKeyDown(int nKey, char cAscii) {
	switch(cAscii){
		case 27:
			this->Close(); // Close Window!
			break;
		case 'g':

			updated = true;
			break;
		case 'G':
			updated = true;

			break;
		default:
			break;
	}
	this->view->key(cAscii, this->width, this->height);
}

void myWindow::OnKeyUp(int nKey, char cAscii) {

	switch(cAscii){
		case 'f':
			//SetFullscreen(true);
			break;
		case 'w':
			//SetFullscreen(false);
			break;
		case '1':
			this->view = this->views[0];
			break;
		case '2':
			this->view = this->views[1];
			break;
		case '3':
			this->view = this->views[2];
			break;
		default:
			break;
	}

	OnRender();
}
;
