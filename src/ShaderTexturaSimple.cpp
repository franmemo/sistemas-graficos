#include "ShaderTexturaSimple.h"
#include "MatrixSetterRender.h"

ShaderTexturaSimple::ShaderTexturaSimple() {
    glShadeModel(GL_SMOOTH);
    glEnable(GL_DEPTH_TEST);
	compileShaderVertex("shaders/texture.vert");
	compileShaderFragment("shaders/texture.frag");
	glBindAttribLocation(handle, 0, "VertexPosition");
	glBindAttribLocation(handle, 1, "VertexNormal");
	glBindAttribLocation(handle, 2, "VertexTexCoord");
	ms = new MatrixSetterRender();
}

ShaderTexturaSimple::~ShaderTexturaSimple(void) {
	delete ms;
}


