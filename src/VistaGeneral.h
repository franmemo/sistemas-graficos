#ifndef __VISTA_GENERAL_H
#define __VISTA_GENERAL_H 

#pragma once 
#include "Vista.h"

class VistaGeneral: public Vista{
public:
	void moveMouse(int x, int y, int max_x, int max_y);
	void key(char cAscii, int max_x, int max_y);

	VistaGeneral(float eyeMod,float angXY,float angXZ, const glm::vec3 at);
	~VistaGeneral();

private:
	float angXY, angXZ, eyeMod;
};

#endif /* __VISTA_GENERAL_H  */
