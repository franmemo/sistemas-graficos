#pragma once

#include "Objeto.h"
#include "Superficie.h"
#include "Curva.h"
#include <vector>
#include <glm/glm.hpp> 

class SuperficieRevolucion : public Superficie
{
public:
	SuperficieRevolucion(Curva& c, unsigned int discretizacion, float resolucion = 1.0f);
	~SuperficieRevolucion(void);
protected:
	void generarMalla();
private:
	glm::vec3 evaluar(float u, float v);
	Curva& c;
};

