/*
 * FrameBuffer.cpp
 *
 *  Created on: 15/08/2013
 *      Author: nacho
 */

#include "FrameBuffer.h"

FrameBuffer::FrameBuffer() {
    GLfloat border[] = {1.0f, 0.0f,0.0f,0.0f };
    // The depth buffer texture
    glGenTextures(1, &depthTex);
    glBindTexture(GL_TEXTURE_2D, depthTex);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, 3*800,
                 3*600, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, border);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LESS);

    // Assign the depth buffer texture to texture channel 0
    glActiveTexture(GL_TEXTURE0 + SHADOWMAP);
    glBindTexture(GL_TEXTURE_2D, depthTex);

    // Create and set up the FBO
    glGenFramebuffers(1, &shadowFBO);
    glBindFramebuffer(GL_FRAMEBUFFER, shadowFBO);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                           GL_TEXTURE_2D, depthTex, 0);

    GLenum drawBuffers[] = {GL_NONE};
    glDrawBuffers(1, drawBuffers);

    GLenum result = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if( result != GL_FRAMEBUFFER_COMPLETE) {
       throw ("Framebuffer is not complete.\n");
    }
    glBindFramebuffer(GL_FRAMEBUFFER,0);
    binded = false;
}

void FrameBuffer::bind() {
	if (binded) return;
	binded = true;
	glBindFramebuffer(GL_FRAMEBUFFER, shadowFBO);
}

void FrameBuffer::unbind() {
	if (!binded) return;
	binded = false;
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
    // Assign the depth buffer texture to texture channel 0
    glActiveTexture(GL_TEXTURE0 + SHADOWMAP);
    glBindTexture(GL_TEXTURE_2D, depthTex);
}


FrameBuffer::~FrameBuffer() {
}

