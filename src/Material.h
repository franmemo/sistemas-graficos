#ifndef MATERIAL_H
#define MATERIAL_H

#include "Shader.h"
#include <glm/glm.hpp>
#include <list>
#include "Propiedad.h"

class Material {
public:
	Material(float red, float green, float blue, float shininess);
	virtual ~Material();
	void addPropiedad(Propiedad* propiedad);
	void setMaterialProperties(Shader* shader);

	void setKa(glm::vec3 ka);

	void setKd(glm::vec3 kd);

	void setKs(glm::vec3 ks);

	void setShininess(float shininess);

	glm::vec3 getKa() const;

	glm::vec3 getKd() const;

	glm::vec3 getKs() const;

	float getShininess() const;

private:
	std::list<Propiedad*> propiedades;
	glm::vec3 Kd;
	glm::vec3 Ks;
	glm::vec3 Ka;
	float shininess;
};

#endif // MATERIAL_H
