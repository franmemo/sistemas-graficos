/*
 * MatrixSetterShadow.h
 *
 *  Created on: 14/08/2013
 *      Author: nacho
 */

#ifndef MATRIXSETTERSHADOW_H_
#define MATRIXSETTERSHADOW_H_
#include "MatrixSetter.h"

class MatrixSetterShadow: public MatrixSetter {
public:
	MatrixSetterShadow();
	virtual ~MatrixSetterShadow();
	virtual void setMatrixes(PropertySetter* ps, Luz* luz, Vista* vista, glm::mat4 model_matrix);
};

#endif /* MATRIXSETTERSHADOW_H_ */
