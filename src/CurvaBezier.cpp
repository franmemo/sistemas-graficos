#include "CurvaBezier.h"

CurvaBezier::CurvaBezier(int pasos):Curva(pasos)
{
}

CurvaBezier::CurvaBezier(glm::vec3 p, int pasos):Curva(p,pasos)
{
}

CurvaBezier::CurvaBezier(std::vector<glm::vec3> &puntos, int pasos):Curva(puntos, pasos)
{
}

CurvaBezier::~CurvaBezier()
{
}

glm::vec3 CurvaBezier::siguiente(){
	glm::vec3 p0 = puntos.at(p0Actual);
	glm::vec3 p1 = puntos.at(p0Actual+1);
	glm::vec3 p2 = puntos.at(p0Actual+2);
	glm::vec3 p3 = puntos.at(p0Actual+3);
	
	glm::vec3 sigue = p0 * b0(u) + p1 * b1(u) + p2 * b2(u) + p3 * b3(u);
	u += (1.0f/pasos);
	if (u >= 1) {
		u = 1;
		if (p0Actual != puntos.size() - 4) {
			p0Actual += 4;
			u = 0;
		}
	}
	return sigue;
}

float CurvaBezier::b0(float u){
	return ((1.f - u) * (1.f - u) * (1.f - u));
}

float CurvaBezier::b1(float u){
	return (3.f * u * (1.f - u) * (1.f - u));
}

float CurvaBezier::b2(float u){
	return (3.f * u * u * (1.f - u));
}

float CurvaBezier::b3(float u){
	return (u * u * u);
}

