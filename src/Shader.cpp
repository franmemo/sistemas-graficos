#include "Shader.h"
#include <iostream>
#include <fstream>
using std::ifstream;
using std::ios;

#include <sstream>
using std::ostringstream;
#include <sys/stat.h>
#include "CacheShaders.h"

FrameBuffer* Shader::fbo = NULL;

void Shader::initFBO() {
	if (!fbo)
		fbo = new FrameBuffer();
}

void Shader::setupFBO() {
	if (!fbo || FBOsetup) return;
	//fbo->unbind();
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//glViewport(0,0,800,600);
	FBOsetup = true;
}

void Shader::teardownFBO() {
	/*if (!fbo) return;
	fbo->unbind();*/
	FBOsetup = false;
}

Shader::Shader() :
	handle(0),
	linked(false),
	FBOsetup(false),
	light_position(glm::vec4(1000, -800, 1000, 1)),
	vertShader(0),
	fragShader(0){
		handle = glCreateProgram();
		if (0 == handle) {
			std::cout << "Error creating program object" << std::endl;
			exit(1);
		}
		ms = NULL;
}

bool Shader::compileShaderVertex( const char * shaderSrcPath)
{
	/////////////////////////////////////////////////////////////
	// Load vertex Shader
	vertShader = glCreateShader(GL_VERTEX_SHADER);
	if (0 == vertShader) {
		std::cout << "Error creating vertex shader" << std::endl;
		exit(1);
	}

	std::ifstream v_shader_file(shaderSrcPath, std::ifstream::in);
	std::string v_str((std::istreambuf_iterator<char>(v_shader_file)),
			std::istreambuf_iterator<char>());
	const char* vs_code_array[] = { v_str.c_str() };

	glShaderSource(vertShader, 1, vs_code_array, NULL);

	// Compilar el shader
	glCompileShader(vertShader);

	// verificar resultado de la compilacion
	GLint vs_compilation_result;
	glGetShaderiv(vertShader, GL_COMPILE_STATUS, &vs_compilation_result);
	if (GL_FALSE == vs_compilation_result) {
		std::cout << "Vertex shader compilation failed!: "<< shaderSrcPath << std::endl;
		GLint logLen;
		glGetShaderiv(vertShader, GL_INFO_LOG_LENGTH, &logLen);
		if (logLen > 0) {
			char * log = (char *) malloc(logLen);
			GLsizei written;
			glGetShaderInfoLog(vertShader, logLen, &written, log);
			std::cout << "Shader log: " << log << std::endl;
			free(log);
		}
		exit(1);
	}
        glAttachShader(handle, vertShader);
        return true;

}

bool Shader::compileShaderFragment( const char * shaderSrcPath)
{
	fragShader = glCreateShader(GL_FRAGMENT_SHADER);
	if (0 == fragShader) {
		std::cout << "Error creating fragment shader" << std::endl;
		exit(1);
	}

	std::ifstream v_shader_file(shaderSrcPath, std::ifstream::in);
	std::string v_str((std::istreambuf_iterator<char>(v_shader_file)),
			std::istreambuf_iterator<char>());
	const char* vs_code_array[] = { v_str.c_str() };

	glShaderSource(fragShader, 1, vs_code_array, NULL);


	// Compilar el shader
	glCompileShader(fragShader);

	// verificar resultado de la compilacion
	GLint fs_compilation_result;
	glGetShaderiv(fragShader, GL_COMPILE_STATUS,
			&fs_compilation_result);

	if (GL_FALSE == fs_compilation_result) {
		std::cout << "Fragment shader compilation failed!\n" << std::endl;
		GLint logLen;
		glGetShaderiv(fragShader, GL_INFO_LOG_LENGTH, &logLen);
		if (logLen > 0) {
			char * log = (char *) malloc(logLen);
			GLsizei written;
			glGetShaderInfoLog(fragShader, logLen, &written, log);
			std::cout << "Shader log: " << log << std::endl;
			free(log);
		}
		exit(1);
	}
        glAttachShader(handle, fragShader);
        return true;
}

bool Shader::link()
{
    if( linked ) return true;
    if( handle <= 0 ) {
		std::cout << "Failed to link shader program!: no program" << std::endl;
		exit(1);
		return false;
	}
	glLinkProgram(handle);
	GLint status;
	glGetProgramiv(handle, GL_LINK_STATUS, &status);
	if (GL_FALSE == status) {
		std::cout << "Failed to link shader program!: can't link" << std::endl;
		GLint logLen;
		glGetProgramiv(handle, GL_INFO_LOG_LENGTH, &logLen);
		if (logLen > 0) {
			char * log = (char *) malloc(logLen);
			GLsizei written;
			glGetProgramInfoLog(handle, logLen, &written, log);
			std::cout << "Program log: \n" << std::string(log) << std::endl;
			free(log);
		}
		exit(1);
		linked = false;
	} else {
		linked = true;
	}
	return linked;
}

void Shader::use()
{
    if( handle <= 0 || (! linked)  || CacheShaders::getInstance().isActive(this)) return;
    glUseProgram( handle );
    CacheShaders::getInstance().activate(this);
    if (fbo) {
    	setupFBO();
    }
}

bool Shader::validate()
{
    if( ! isLinked() ) {
		std::cout << "Validation error.";
		exit(1);
		return false;
	}

    GLint status;
    glValidateProgram( handle );
    glGetProgramiv( handle, GL_VALIDATE_STATUS, &status );

    if( GL_FALSE == status ) {
        // Store log and return false
        int length = 0;
        glGetProgramiv(handle, GL_INFO_LOG_LENGTH, &length );

        if( length > 0 ) {
            char * c_log = new char[length];
            int written = 0;
            glGetProgramInfoLog(handle, length, &written, c_log);
            delete [] c_log;
        }
		exit(1);
        return false;
    } else {
       return true;
    }
}

int Shader::getHandle()
{
    return handle;
}

bool Shader::isLinked()
{
    return linked;
}

void Shader::bindAttribLocation( GLuint location, const char * name)
{
    glBindAttribLocation(handle, location, name);
}

void Shader::bindFragDataLocation( GLuint location, const char * name )
{
    glBindFragDataLocation(handle, location, name);
}

void Shader::setUniform( const char *name, float x, float y, float z)
{
    int loc = getUniformLocation(name);
    if( loc >= 0 ) {
        glUniform3f(loc,x,y,z);
    } else {
        printf("Uniform: %s not found.\n",name);
    }
}

void Shader::setUniform( const char *name, const glm::vec3 & v)
{
    this->setUniform(name,v.x,v.y,v.z);
}

void Shader::setUniform( const char *name, const glm::vec4 & v)
{
    int loc = getUniformLocation(name);
    if( loc >= 0 ) {
        glUniform4f(loc,v.x,v.y,v.z,v.w);
    } else {
        printf("Uniform: %s not found.\n",name);
    }
}

void Shader::setUniform( const char *name, const glm::vec2 & v)
{
    int loc = getUniformLocation(name);
    if( loc >= 0 ) {
        glUniform2f(loc,v.x,v.y);
    } else {
        printf("Uniform: %s not found.\n",name);
    }
}

void Shader::setUniform( const char *name, const glm::mat4 & m)
{
    int loc = getUniformLocation(name);
    if( loc >= 0 )
    {
        glUniformMatrix4fv(loc, 1, GL_FALSE, &m[0][0]);
    } else {
        printf("Uniform: %s not found.\n",name);
    }
}

void Shader::setUniform( const char *name, const glm::mat3 & m)
{
    int loc = getUniformLocation(name);
    if( loc >= 0 )
    {
        glUniformMatrix3fv(loc, 1, GL_FALSE, &m[0][0]);
    } else {
        printf("Uniform: %s not found.\n",name);
    }
}

void Shader::setUniform( const char *name, float val )
{
    int loc = getUniformLocation(name);
    if( loc >= 0 )
    {
        glUniform1f(loc, val);
    } else {
        printf("Uniform: %s not found.\n",name);
    }
}

void Shader::setUniform( const char *name, int val )
{
    int loc = getUniformLocation(name);
    if( loc >= 0 )
    {
        glUniform1i(loc, val);
    } else {
        printf("Uniform: %s not found.\n",name);
    }
}

void Shader::setUniform( const char *name, bool val )
{
    int loc = getUniformLocation(name);
    if( loc >= 0 )
    {
        glUniform1i(loc, val);
    } else {
        printf("Uniform: %s not found.\n",name);
    }
}

int Shader::getUniformLocation(const char * name )
{
    return glGetUniformLocation(handle, name);
}

void Shader::setMatrixes(Luz* luz, Vista* vista, glm::mat4 model_matrix) {
	ms->setMatrixes(this, luz, vista, model_matrix);
}

Shader::~Shader(){

}

