#include "Cubo.h"
#include "Imagen.h"


static float positionData[] =
{
	0.5f, -0.5f,  0.5f,
	0.5f, -0.5f, -0.5f,
	0.5f,  0.5f, -0.5f,
	0.5f,  0.5f,  0.5f,

	0.5f,  0.5f,  0.5f,
	0.5f,  0.5f, -0.5f,
	-0.5f,  0.5f, -0.5f,
	-0.5f,  0.5f,  0.5f,

	-0.5f,  0.5f,  0.5f,
	-0.5f,  0.5f, -0.5f,
	-0.5f, -0.5f, -0.5f,
	-0.5f, -0.5f,  0.5f,

	0.5f, -0.5f,  0.5f,
	0.5f, -0.5f, -0.5f,
	-0.5f, -0.5f, -0.5f,
	-0.5f, -0.5f,  0.5f,

	0.5f,  0.5f,  0.5f,
	0.5f, -0.5f,  0.5f,
	-0.5f, -0.5f,  0.5f,
	-0.5f,  0.5f,  0.5f,

	0.5f,  0.5f, -0.5f,
	-0.5f,  0.5f, -0.5f,
	-0.5f, -0.5f, -0.5f,
	0.5f, -0.5f, -0.5f
};

static float normalData[] =
{
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,
	1.0f,  0.0f,  0.0f,

	0.0f,  1.0f,  0.0f,
	0.0f,  1.0f,  0.0f,
	0.0f,  1.0f,  0.0f,
	0.0f,  1.0f,  0.0f,

	-1.0f,  0.0f,  0.0f,
	-1.0f,  0.0f,  0.0f,
	-1.0f,  0.0f,  0.0f,
	-1.0f,  0.0f,  0.0f,

	0.0f, -1.0f,  0.0f,
	0.0f, -1.0f,  0.0f,
	0.0f, -1.0f,  0.0f,
	0.0f, -1.0f,  0.0f,

	0.0f,  0.0f,  1.0f,
	0.0f,  0.0f,  1.0f,
	0.0f,  0.0f,  1.0f,
	0.0f,  0.0f,  1.0f,

	0.0f,  0.0f, -1.0f,
	0.0f,  0.0f, -1.0f,
	0.0f,  0.0f, -1.0f,
	0.0f,  0.0f, -1.0f
};

static float textureData[] =
{
	0.0f,  0.0f,
	1.0f,  0.0f,
	1.0f,  1.0f,
	0.0f,  1.0f,

	0.0f,  0.0f,
	1.0f,  0.0f,
	1.0f,  1.0f,
	0.0f,  1.0f,

	0.0f,  0.0f,
	1.0f,  0.0f,
	1.0f,  1.0f,
	0.0f,  1.0f,

	0.0f,  0.0f,
	1.0f,  0.0f,
	1.0f,  1.0f,
	0.0f,  1.0f,

	0.0f,  0.0f,
	1.0f,  0.0f,
	1.0f,  1.0f,
	0.0f,  1.0f,

	0.0f,  0.0f,
	1.0f,  0.0f,
	1.0f,  1.0f,
	0.0f,  1.0f,
};


static float tangentData[] =
{
	0.0f,  1.0f,  0.0f, 1.0f,
	0.0f,  1.0f,  0.0f, 1.0f,
	0.0f,  1.0f,  0.0f, 1.0f,
	0.0f,  1.0f,  0.0f, 1.0f,

	-1.0f,  0.0f,  0.0f, 1.0f,
	-1.0f,  0.0f,  0.0f, 1.0f,
	-1.0f,  0.0f,  0.0f, 1.0f,
	-1.0f,  0.0f,  0.0f, 1.0f,

	0.0f, -1.0f,  0.0f, 1.0f,
	0.0f, -1.0f,  0.0f, 1.0f,
	0.0f, -1.0f,  0.0f, 1.0f,
	0.0f, -1.0f,  0.0f, 1.0f,

	1.0f,  0.0f,  0.0f, 1.0f,
	1.0f,  0.0f,  0.0f, 1.0f,
	1.0f,  0.0f,  0.0f, 1.0f,
	1.0f,  0.0f,  0.0f, 1.0f,

	-1.0f,  0.0f,  0.0f, 1.0f,
	-1.0f,  0.0f,  0.0f, 1.0f,
	-1.0f,  0.0f,  0.0f, 1.0f,
	-1.0f,  0.0f,  0.0f, 1.0f,

	1.0f,  0.0f,  0.0f, 1.0f,
	1.0f,  0.0f,  0.0f, 1.0f,
	1.0f,  0.0f,  0.0f, 1.0f,
	1.0f,  0.0f,  0.0f,	1.0f,
};

Cubo::Cubo(float resolucion){

	glGenVertexArrays( 1, &vaoHandle );
	glBindVertexArray( vaoHandle );

	GLuint vboHandles[4];
	glGenBuffers(4, vboHandles);

	GLuint positionBufferHandle = vboHandles[0];
	GLuint normalBufferHandle = vboHandles[1];
	GLuint textureBufferHandle = vboHandles[2];
	GLuint tangenteBufferHandle = vboHandles[3];

	glBindBuffer( GL_ARRAY_BUFFER, positionBufferHandle );
	glBufferData( GL_ARRAY_BUFFER, 3* 24 * sizeof (float), positionData, GL_STATIC_DRAW );
	glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, (GLubyte*)NULL);
	glEnableVertexAttribArray(0);

	glBindBuffer( GL_ARRAY_BUFFER, normalBufferHandle );
	glBufferData( GL_ARRAY_BUFFER, 3* 24 * sizeof (float), normalData, GL_STATIC_DRAW );
	glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, 0, (GLubyte*)NULL);
	glEnableVertexAttribArray(1);

	float texData[2 * 24];

	for (int i = 0; i < 2 * 24; i++) {
		texData[i] = textureData[i] * resolucion;
	}

	glBindBuffer( GL_ARRAY_BUFFER, textureBufferHandle );
	glBufferData( GL_ARRAY_BUFFER, 2* 24 * sizeof (float), texData, GL_STATIC_DRAW );
	glVertexAttribPointer( 2, 2, GL_FLOAT, GL_FALSE, 0, (GLubyte*)NULL);
	glEnableVertexAttribArray(2);

	glBindBuffer( GL_ARRAY_BUFFER, tangenteBufferHandle );
	glBufferData( GL_ARRAY_BUFFER, 4 * 24 * sizeof (float), tangentData, GL_STATIC_DRAW );
	glVertexAttribPointer( 3, 4, GL_FLOAT, GL_FALSE, 0, (GLubyte*)NULL);
	glEnableVertexAttribArray(3);

	glBindVertexArray(0);
}

Cubo::~Cubo(void)
{
}

void Cubo::render(){
	glBindVertexArray( vaoHandle );
	glDrawArrays(GL_QUADS, 0, 24);
}
