#ifndef __CILINDRO_CPP
#define __CILINDRO_CPP
#include "Cilindro.h"
#include <cmath>
#include <vector>
#include <iostream>

#define ARRIBA (1)

#define ABAJO (-1)

void Cilindro::render() {
	glBindVertexArray( vaoHandle );
	glDrawArrays(GL_QUADS, 0, segmentos * 4 * 2 * 2);
}

void Cilindro::crearCara(const float phi, unsigned& ivec, unsigned& itex, unsigned& inorm, unsigned& itang) {
	float senPhi = std::sin(phi);
	float cosPhi = std::cos(phi);
	float auxphi = phi / (2 * PI);
	float senPhi2 = std::sin(phi + 2 * PI / segmentos);
	float cosPhi2 = std::cos(phi + 2 * PI / segmentos);
	float auxphi2 = auxphi + 1.f /(float) segmentos;

	// primer punto (abajo a la izquierda)
	glm::vec3 v1(0.f);
	glm::vec3 n1(0.f);

	vertex_buffer[ivec++] = v1.x = radio * cosPhi;
	normal_buffer[inorm++] = n1.x = cosPhi;
	vertex_buffer[ivec++] = v1.y = radio * senPhi;
	normal_buffer[inorm++] = n1.y = senPhi;
	vertex_buffer[ivec++] = v1.z = -altura / 2;
	normal_buffer[inorm++] = n1.z = 0;
	texture_buffer[itex++] = auxphi;
	texture_buffer[itex++] = 0;

	// segundo punto (abajo a la derecha)
	glm::vec3 v2(0.f);
	glm::vec3 n2(0.f);

	vertex_buffer[ivec++] = v2.x = radio * cosPhi2;
	normal_buffer[inorm++] = n2.x = cosPhi2;
	vertex_buffer[ivec++] = v2.y = radio * senPhi2;
	normal_buffer[inorm++] = n2.y = senPhi2;
	vertex_buffer[ivec++] = v2.z = -altura / 2;
	normal_buffer[inorm++] = n2.z = 0;
	texture_buffer[itex++] = auxphi2;
	texture_buffer[itex++] = 0;

	// tercer punto (arriba a la derecha)
	glm::vec3 v3(0.f);
	glm::vec3 n3(0.f);

	vertex_buffer[ivec++] = v3.x = radio * cosPhi2;
	normal_buffer[inorm++] = n3.x = cosPhi2;
	vertex_buffer[ivec++] = v3.y = radio * senPhi2;
	normal_buffer[inorm++] = n3.y = senPhi2;
	vertex_buffer[ivec++] = v3.z = altura / 2;
	normal_buffer[inorm++] = n3.z = 0;
	texture_buffer[itex++] = auxphi2;
	texture_buffer[itex++] = altura;

	// cuarto punto (arriba a la izquierda)
	glm::vec3 v4(0.f);
	glm::vec3 n4(0.f);

	vertex_buffer[ivec++] = v4.x = radio * cosPhi;
	normal_buffer[inorm++] = n4.x = cosPhi;
	vertex_buffer[ivec++] = v4.y = radio * senPhi;
	normal_buffer[inorm++] = n4.y = senPhi;
	vertex_buffer[ivec++] = v4.z = altura / 2;
	normal_buffer[inorm++] = n4.z = 0;
	texture_buffer[itex++] = auxphi;
	texture_buffer[itex++] = altura;

	// tangente del primero
	glm::vec3 t = v2 - v1;
	t = glm::normalize(t - n1 * glm::dot(n1, t));

	tangent_buffer[itang++] = t.x;
	tangent_buffer[itang++] = t.y;
	tangent_buffer[itang++] = t.z;
	tangent_buffer[itang] = 1;

	// tangente del segundo
	glm::vec3 v2d(
				std::cos(phi + 2.0f*( 2 * PI )/segmentos),
				std::sin(phi + 2.0f*( 2 * PI )/segmentos),
				-altura/2
				);

	t = v2d - v2;
	t = glm::normalize(t - n2 * glm::dot(n2, t));

	tangent_buffer[itang++] = t.x;
	tangent_buffer[itang++] = t.y;
	tangent_buffer[itang++] = t.z;
	tangent_buffer[itang] = 1;

	// tangente del tercero
	glm::vec3 v3d(
					std::cos(phi + 2.0f*( 2 * PI )/segmentos),
					std::sin(phi + 2.0f*( 2 * PI )/segmentos),
					altura/2
					);

	t = v3d - v3;
	t = glm::normalize(t - n2 * glm::dot(n3, t));

	tangent_buffer[itang++] = t.x;
	tangent_buffer[itang++] = t.y;
	tangent_buffer[itang++] = t.z;
	tangent_buffer[itang] = 1;

	// tangente del cuarto
	t = glm::normalize( v3 - v4);
	t = glm::normalize(t - n4 * glm::dot(n4, t));

	tangent_buffer[itang++] = t.x;
	tangent_buffer[itang++] = t.y;
	tangent_buffer[itang++] = t.z;
	tangent_buffer[itang] = 1;

}

void Cilindro::crearTapa(int sentido, unsigned& ivec, unsigned& itex, unsigned& inorm, unsigned& itang) {
	float phi;
	float h = sentido * altura / 2;
	for (int j=0; j < segmentos; ++j) {
		phi = j * (2 * PI) / segmentos;
		float x, y, z;
		// primer punto (arriba a la izquierda)

		vertex_buffer[ivec++] = 0;
		vertex_buffer[ivec++] = 0;
		vertex_buffer[ivec++] = h;

		normal_buffer[inorm++] = 0;
		normal_buffer[inorm++] = 0;
		normal_buffer[inorm++] = sentido;

		texture_buffer[itex++] = 0.5;
		texture_buffer[itex++] = 0.5;

		tangent_buffer[itang++] = 0;
		tangent_buffer[itang++] = 1;
		tangent_buffer[itang++] = 0;
		tangent_buffer[itang++] = 1;

		//segundo punto (abajo a la izquierda)

		vertex_buffer[ivec++] = radio *  std::cos(phi);
		vertex_buffer[ivec++] = radio *  std::sin(phi);
		vertex_buffer[ivec++] = h;

		normal_buffer[inorm++] = 0;
		normal_buffer[inorm++] = 0;
		normal_buffer[inorm++] = sentido;

		texture_buffer[itex++] =(0.5f + 0.5f * std::cos(phi));
		texture_buffer[itex++] =(0.5f + 0.5f * std::sin(phi));

		tangent_buffer[itang++] = 0;
		tangent_buffer[itang++] = 1;
		tangent_buffer[itang++] = 0;
		tangent_buffer[itang++] = 1;


		// Tercer punto (abajo a la derecha)
		phi += 2*PI/segmentos;

		vertex_buffer[ivec++] = radio * std::cos(phi);
		vertex_buffer[ivec++] = radio * std::sin(phi);
		vertex_buffer[ivec++] = h;

		normal_buffer[inorm++] = 0;
		normal_buffer[inorm++] = 0;
		normal_buffer[inorm++] = sentido;

		texture_buffer[itex++] =(0.5f + 0.5f * std::cos(phi));
		texture_buffer[itex++] =(0.5f + 0.5f * std::sin(phi));

		tangent_buffer[itang++] = 0;
		tangent_buffer[itang++] = 1;
		tangent_buffer[itang++] = 0;
		tangent_buffer[itang++] = 1;

		// cuarto punto (origen)
		vertex_buffer[ivec++] = 0;
		vertex_buffer[ivec++] = 0;
		vertex_buffer[ivec++] = h;

		normal_buffer[inorm++] = 0;
		normal_buffer[inorm++] = 0;
		normal_buffer[inorm++] = sentido;

		texture_buffer[itex++] = 0.5f;
		texture_buffer[itex++] = 0.5f;

		tangent_buffer[itang++] = 0;
		tangent_buffer[itang++] = 1;
		tangent_buffer[itang++] = 0;
		tangent_buffer[itang++] = 1;
	}
}

Cilindro::Cilindro(const float radio, const float altura, const unsigned int segmentos):
	radio(radio),
	altura(altura),
	segmentos(segmentos) {
	assert(radio > 0.0 && altura > 0.0 && segmentos > 0);
	this->vertex_buffer_size = segmentos * 4 * 3 * 3;
	this->vertex_buffer = new GLfloat[this->vertex_buffer_size];
	this->normal_buffer_size = vertex_buffer_size;
	this->normal_buffer = new GLfloat[this->normal_buffer_size];
	this->tangent_buffer_size = segmentos * 4 * 4 * 3;
	this->tangent_buffer = new GLfloat[this->tangent_buffer_size];
	this->texture_buffer_size = segmentos * 4 * 3 * 2;
	this->texture_buffer = new GLfloat[this->texture_buffer_size];

	float d_altura = altura/-2.;
	unsigned ivec = 0;
	unsigned itex = 0;
	unsigned inorm = 0;
	unsigned itang = 0;
	for(unsigned int seg = 0; seg < segmentos; ++seg) {
		float phi = seg * 2 * PI / segmentos;
		crearCara(phi, ivec, itex, inorm, itang);
	}

	crearTapa(ABAJO, ivec, itex, inorm, itang);
	crearTapa(ARRIBA, ivec, itex, inorm, itang);


	glGenVertexArrays( 1, &vaoHandle );
	glBindVertexArray( vaoHandle );

	GLuint vboHandles[4];
	glGenBuffers(4, vboHandles);

	GLuint positionBufferHandle = vboHandles[0];
	GLuint normalBufferHandle = vboHandles[1];
	GLuint textureBufferHandle = vboHandles[2];
	GLuint tangenteBufferHandle = vboHandles[3];

	glBindBuffer( GL_ARRAY_BUFFER, positionBufferHandle );
	glBufferData( GL_ARRAY_BUFFER, vertex_buffer_size * sizeof (float), vertex_buffer, GL_STATIC_DRAW );
	glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, (GLubyte*)NULL);
	glEnableVertexAttribArray(0);

	glBindBuffer( GL_ARRAY_BUFFER, normalBufferHandle );
	glBufferData( GL_ARRAY_BUFFER, normal_buffer_size * sizeof (float), normal_buffer, GL_STATIC_DRAW );
	glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, 0, (GLubyte*)NULL);
	glEnableVertexAttribArray(1);

	glBindBuffer( GL_ARRAY_BUFFER, textureBufferHandle );
	glBufferData( GL_ARRAY_BUFFER, texture_buffer_size * sizeof (float), texture_buffer, GL_STATIC_DRAW );
	glVertexAttribPointer( 2, 2, GL_FLOAT, GL_FALSE, 0, (GLubyte*)NULL);
	glEnableVertexAttribArray(2);

	glBindBuffer( GL_ARRAY_BUFFER, tangenteBufferHandle );
	glBufferData( GL_ARRAY_BUFFER, tangent_buffer_size * sizeof(float), tangent_buffer, GL_STATIC_DRAW );
	glVertexAttribPointer( 3, 4, GL_FLOAT, GL_FALSE, 0, (GLubyte*)NULL);
	glEnableVertexAttribArray(3);

	glBindVertexArray(0);

}

Cilindro::~Cilindro() {
}

#endif
