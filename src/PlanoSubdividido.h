#ifndef __PLANOSUBDIVIDIDO_H
#define __PLANOSUBDIVIDIDO_H 

#include "Primitiva.h"
#pragma once



class PlanoSubdividido: public Primitiva{
public:
	PlanoSubdividido(int segmentosPorArista = 10, float resolucion = 1.0f);
	~PlanoSubdividido();
	virtual void render();

private:
	int	segmentosPorArista;
};


#endif /* __PLANOSUBDIVIDIDO_H */
