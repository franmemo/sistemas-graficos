/*
 * SuperficieBarrido.cpp
 *
 *  Created on: 04/07/2013
 *      Author: nacho
 */

#include "SuperficieBarrido.h"
#include <iostream>

void SuperficieBarrido::generarMalla() {
	glm::vec3 desplz = glm::vec3(0, 0, altura_lateral / 2);
	std::vector<glm::vec3> curva = recorrido.vector();
	for (unsigned int i = 0; i < curva.size() - 1; i++){
		glm::vec3 norm = curva[i+1] - curva[i];
		glm::vec3 tmp = glm::normalize(glm::cross(norm, this->normal));
		tmp *= ancho_lateral/2;
		puntos.push_back(curva[i] + tmp + desplz);
		puntos.push_back(curva[i] - tmp + desplz);
		puntos.push_back(curva[i] - tmp - desplz);
		puntos.push_back(curva[i] + tmp - desplz);
		puntos.push_back(curva[i] + tmp + desplz);

		normales.push_back(tmp + desplz);
		normales.push_back(- tmp + desplz);
		normales.push_back(- tmp - desplz);
		normales.push_back(tmp - desplz);
		normales.push_back(tmp + desplz);
	
		texturas.push_back(glm::vec2(i*0.1f,0.f));
		texturas.push_back(glm::vec2(i*0.1f,0.25f));
		texturas.push_back(glm::vec2(i*0.1f,0.5f));
		texturas.push_back(glm::vec2(i*0.1f,0.75f));
		texturas.push_back(glm::vec2(i*0.1f,1.f));
	}
	alto = puntos.size() / 5;
	ancho = 5;
}

SuperficieBarrido::SuperficieBarrido(float ancho_lateral,
		float altura_lateral, Curva& recorrido, glm::vec3& normal):
	Superficie(0),
	recorrido(recorrido),
	ancho_lateral(ancho_lateral),
	altura_lateral(altura_lateral),
	normal(normal)
	{
	generarMalla();
	generarTriangulos();
}


SuperficieBarrido::~SuperficieBarrido() {
}

