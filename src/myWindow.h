#pragma once

#include "glutWindow.h"
#include <glm/glm.hpp> 
#include <glm/gtc/matrix_transform.hpp> 


#include "Mundo.h"
#include "Luz.h"
#include "VistaGeneral.h"
#include "VistaSuelo.h"
#include <vector>


class myWindow: public cwc::glutWindow {
public:

	myWindow();
	~myWindow();

	virtual void OnRender(void);
	virtual void OnIdle();

	// When OnInit is called, a render context (in this case GLUT-Window) 
	// is already available!
	virtual void OnInit();

	virtual void OnResize(int w, int h);
	virtual void OnClose(void);
	virtual void OnMouseDown(int button, int x, int y);
	virtual void OnMouseUp(int button, int x, int y);
	virtual void OnMouseWheel(int nWheelNumber, int nDirection, int x, int y);
	virtual void OnMouseMove(int x, int y);
	virtual void OnLeftMouseDrag(int x, int y);
	virtual void OnKeyDown(int nKey, char cAscii);
	virtual void OnKeyUp(int nKey, char cAscii);

private:
	glm::mat4 view_matrix;
	bool init;
	bool updated;
	Mundo* mundo;

	std::vector<Vista*> views;
	Vista* view;
	Luz luz;
	float tiempo;
};
