/*
 * MatrixSetter.h
 *
 *  Created on: 14/08/2013
 *      Author: nacho
 */

#ifndef MATRIXSETTER_H_
#define MATRIXSETTER_H_

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "Definiciones.h"
#include "Luz.h"
#include "Vista.h"
#include "PropertySetter.h"

class MatrixSetter {
public:
	MatrixSetter();
	virtual ~MatrixSetter();
	virtual void setMatrixes(PropertySetter* propertySetter, Luz* luz, Vista* vista, glm::mat4 model_matrix) = 0;
};

#endif /* MATRIXSETTER_H_ */
