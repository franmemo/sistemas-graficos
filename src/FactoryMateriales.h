/*
 * FactoryPropiedades.h
 *
 *  Created on: 06/08/2013
 *      Author: nacho
 */

#ifndef FACTORYPROPIEDADES_H_
#define FACTORYPROPIEDADES_H_

#include "Propiedad.h"
#include "Material.h"

class FactoryMateriales {
public:
	FactoryMateriales();
	virtual ~FactoryMateriales();
	static Material* createCopa();
	static Material* createTronco();
	static Material* createGrua();
	static Material* createColumna();
	static Material* createFondo();
	static Material* createVidrioAfuera();
	static Material* createVidrioAdentro();
	static Material* createParante();
	static Material* createPiso();
	static Material* createTierra();
	static Material* createParded();
	static Material* createMonticulo();
	static Material* createMonticulo2();
	static Material* createDucto();
	static Material* createEscalera();
};

#endif /* FACTORYPROPIEDADES_H_ */
