#include "Curva.h"

Curva::Curva(int pasos)
{
	this->pasos = pasos;
	this->p0Actual = 0;
	u = 0;
}


Curva::Curva(glm::vec3 p, int pasos)
{
	this->puntos.push_back(p);
	this->pasos = pasos;

	this->p0Actual = 0;
	u = 0;
}

Curva::Curva(std::vector<glm::vec3> &puntos, int pasos)
{
	this->pasos = pasos;
	std::vector<glm::vec3>::iterator it;
	for(it= puntos.begin(); it != puntos.end(); it++){
		this->puntos.push_back(*it);
	}

	this->p0Actual = 0;
	u = 0;

}

Curva::~Curva(){
	puntos.clear();
}

bool Curva::tieneSiguiente(){
	return (u < 1 && (puntos.size() - p0Actual) > 4) || !(u == 1 && p0Actual==puntos.size()-4);
}

void Curva::agregarPunto(glm::vec3 s){
	puntos.push_back(s);
}


std::vector<glm::vec3> Curva::vector(){
	std::vector<glm::vec3> datos;
	while (tieneSiguiente()){
		datos.push_back( siguiente() );
	}
	return datos;
}


