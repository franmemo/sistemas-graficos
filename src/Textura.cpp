/*
 * Textura.cpp
 *
 *  Created on: 06/08/2013
 *      Author: nacho
 */

#include "Textura.h"
#include "CacheTexturas.h"
#include <iostream>
#include "Imagen.h"

#include <GL/glew.h>
#include <GL/freeglut.h>

#include <iostream>

#include <cmath>

//TODO: Actualizar segun los nombres del shader
static char* sampler_map[] = { "Tex", "NormalMap", "Shadow"};

Textura::Textura(std::string textura, tipo_textura tipo, float texBlend): texture_id(textura), tipo(tipo), texBlending(texBlend) {
	Imagen *imagen = CacheTexturas::getInstance().cargarTextura(texture_id);
	sampler = sampler_map[tipo];
	tid = imagen->getId();
	glActiveTexture(GL_TEXTURE0 + tipo);
	glBindTexture(GL_TEXTURE_2D, tid);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,  GL_LINEAR_MIPMAP_LINEAR );
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
}

Textura::~Textura() {
	glDeleteTextures(1, (GLuint*)&tid);
}

void Textura::applyPropiedad(Shader* shader, Material* material) {
	glActiveTexture(GL_TEXTURE0 + tipo);
	glBindTexture(GL_TEXTURE_2D, tid);
/*	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,  GL_LINEAR_MIPMAP_LINEAR );*/
	shader->setUniform(sampler, GL_TEXTURE0 + (int)  tipo);
	shader->setUniform("TexBlending", texBlending);
}


