#include "CurvaBSpline.h"

CurvaBSpline::CurvaBSpline(int pasos):Curva(pasos)
{
	pact = glm::vec3(1.0f);
	pant = glm::vec3(1.0f);
}

CurvaBSpline::CurvaBSpline(glm::vec3 p, int pasos):Curva(p,pasos)
{
	pact = glm::vec3(1.0f);
	pant = glm::vec3(1.0f);
}

CurvaBSpline::CurvaBSpline(std::vector<glm::vec3> &puntos, int pasos):Curva(puntos, pasos)
{
	pact = glm::vec3(1.0f);
	pant = glm::vec3(1.0f);
}

CurvaBSpline::~CurvaBSpline()
{
}

glm::vec3 CurvaBSpline::siguiente(){
	glm::vec3 p0 = puntos.at(p0Actual);
	glm::vec3 p1 = puntos.at(p0Actual+1);
	glm::vec3 p2 = puntos.at(p0Actual+2);
	glm::vec3 p3 = puntos.at(p0Actual+3);
	pant = pact;
	glm::vec3 sigue = p0 * b0(u) + p1 * b1(u) + p2 * b2(u) + p3 * b3(u);
	pact = sigue;
	u += (1.0f/pasos);
	if (u >= 1) {
		u = 1;
		if (p0Actual != puntos.size() - 4) {
			p0Actual++;
			u = 0;
		}
	}
	return sigue;
}

glm::vec3 CurvaBSpline::tangente(){
	return (pact - pant) / (1.0f / pasos);
}

float CurvaBSpline::b0(float u){
	return (1 - 3 * u + 3 * u * u - u * u * u) / 6.0f;
}

float CurvaBSpline::b1(float u){
	return (4 - 6 * u * u + 3 * u * u * u) / 6.0f;
}

float CurvaBSpline::b2(float u){
	return (1 + 3 * u + 3 * u * u - 3 * u * u * u) / 6.0f;
}

float CurvaBSpline::b3(float u){
	return (u * u * u) / 6.0f;
}

