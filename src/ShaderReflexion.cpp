#include "ShaderReflexion.h"
#include "MatrixSetterRender.h"

ShaderReflexion::ShaderReflexion()
{
	compileShaderVertex("shaders/reflexion.vert");
	compileShaderFragment("shaders/reflexion.frag");
	glBindAttribLocation(handle, 0, "VertexPosition");
	glBindAttribLocation(handle, 1, "VertexNormal");
	glBindAttribLocation(handle, 2, "VertexTexCoord");
	glBindAttribLocation(handle, 3, "VertexTangent");
	ms = new MatrixSetterRender();

}

ShaderReflexion::~ShaderReflexion(void)
{
	delete ms;
}

