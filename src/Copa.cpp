/*
 * Copa.cpp
 *
 *  Created on: 10/08/2013
 *      Author: nacho
 */

#include "Copa.h"
#include "CurvaBezier.h"
#include "FactoryMateriales.h"
#include "CacheShaders.h"

#include <iostream>
#include <cmath>

Copa::Copa(int dscr): nivelDiscretizacion(dscr) {
	defshader = shader = CacheShaders::getInstance().getShader(std::string("copa"));
	material = FactoryMateriales::createCopa();
	CurvaBezier cCopa(nivelDiscretizacion);
	cCopa.agregarPunto(glm::vec3(0.f,0.f,3.75f));
	cCopa.agregarPunto(glm::vec3(1.f,0.f,3.5f));
	cCopa.agregarPunto(glm::vec3(2.f,0.f,3.5f));
	cCopa.agregarPunto(glm::vec3(1.5f,0.f,2.5f));
	cCopa.agregarPunto(glm::vec3(1.5f,0.f,2.5f));
	cCopa.agregarPunto(glm::vec3(2.f,0.f,2.25f));
	cCopa.agregarPunto(glm::vec3(2.75f,0.f,1.5f));
	cCopa.agregarPunto(glm::vec3(2.f,0.f,0.75f));
	cCopa.agregarPunto(glm::vec3(2.f,0.f,0.75f));
	cCopa.agregarPunto(glm::vec3(1.5f,0.f,0.25f));
	cCopa.agregarPunto(glm::vec3(1.f,0.f,0.5f));
	cCopa.agregarPunto(glm::vec3(0.f,0.f,0.f));
	this->copa = new SuperficieRevolucion(cCopa, this->nivelDiscretizacion, 5.f);
	ponderacion = 0.5;// % 2 + 1.0;
}

Copa::~Copa() {
	delete material;
	delete copa;
}


void Copa::render(Luz* luz, Vista* view, glm::mat4 model_matrix) {
	shader->use();
	if (defshader == shader) {
		shader->setUniform("ponderacion", ponderacion);
		luz->setLightProperties(shader, view);
		material->setMaterialProperties(shader);
	}
	shader->setMatrixes(luz, view, model_matrix);
	copa->render();

}
