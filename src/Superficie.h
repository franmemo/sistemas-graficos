#pragma once

#include "Curva.h"
#include <vector>
#include <glm/glm.hpp> 
#include "Primitiva.h"

class Superficie : public Primitiva
{
public:
	Superficie(unsigned int pasos, float resolucion = 1.0f);
	virtual ~Superficie(void);
	virtual void render();
protected:
	virtual void generarMalla()=0;
	virtual void generarTriangulos();
	unsigned int discretizacion;
	//---------------
	std::vector<glm::vec3> puntos, normales;
	std::vector<glm::vec2> texturas;
	int ancho, alto;
	float resolucion;

};

