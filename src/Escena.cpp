/*
 * Escena.cpp
 *
 *  Created on: 05/07/2013
 *      Author: nacho
 */

#include "Escena.h"
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <cmath>
#include "FactoryMateriales.h"

#define DIST_EDIFICIO 9
#define MAX_DIST 15
#define RANDOM_NUMBER_ARG(FROM, TO) (float)(pow(-1, rand()%2) *(rand() % (TO-FROM) + FROM))
#define RANDOM_NUMBER() RANDOM_NUMBER_ARG(DIST_EDIFICIO, MAX_DIST)


Escena::Escena(int nivelDiscretizacion, int cantArboles, int cantMonticulos,
		int rangoPicos) /*:
		cantArboles(cantArboles) */{
/*
	parts["arbol"] = new Arbol(nivelDiscretizacion);
	parts["monticulo1"] = new Monticulo(MONTICULO_ARENA, nivelDiscretizacion);
	parts["monticulo2"] = new Monticulo(MONTICULO_PIEDRA, nivelDiscretizacion);*/
	srand((unsigned) time(0));
	for (int i = 0; i < cantArboles; ++i) {
		glm::vec3* rvec = new glm::vec3(RANDOM_NUMBER(), RANDOM_NUMBER(), 0);
		//pos_arboles.push_back(rvec);
	}

	cantMonticulos = (cantMonticulos >= 2)? cantMonticulos : 2;

	for (int i = 0; i < cantMonticulos; ++i) {
		glm::vec3* rvec = new glm::vec3(RANDOM_NUMBER(), RANDOM_NUMBER(), 0);
		//pos_monticulos.push_back(rvec);
	}
}

void Escena::renderArboles(Luz *luz, Vista* view, glm::mat4 model_matrix) {
/*	for (int i = 0; i < cantArboles; ++i) {
		parts["arbol"]->render(luz, view, glm::translate(model_matrix, *pos_arboles[i]));
	}*/
}

void Escena::renderMonticulos(Luz* luz, Vista* view, glm::mat4 model_matrix) {
	/*for (int i = 0; i < pos_monticulos.size(); i += 2) {
		parts["monticulo1"]->render(luz, view, glm::translate(model_matrix, *pos_monticulos[i]));
	}*/
}

void Escena::render(Luz* luz, Vista* view, glm::mat4 model_matrix) {
	renderArboles(luz, view, model_matrix);
	renderMonticulos(luz, view, model_matrix);
}

Escena::~Escena() {
}

